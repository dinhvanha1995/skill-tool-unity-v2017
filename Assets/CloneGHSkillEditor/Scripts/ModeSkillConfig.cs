﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum SkillTargetMode
{
    OnlyCharacter,
    EnemyInteraction,
}
public enum CameraViewMode
{
    InFront,
    Behind,
    Right,
    Left
}
public enum SkillElementType
{
    Damage,
    Particles,
    Function,
}
public enum SkillTargetType
{
    MySelf,
    Enemy
}
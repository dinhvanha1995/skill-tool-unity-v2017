﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;
public class GHSkillTool : EditorWindow
{
    public Texture aTexture;
    public int height = 0;
    public Rect rectSlider = new Rect(-10, 5, 20, 20);

    bool b;
    [MenuItem("Examples/Texture Previewer")]
    static void Init()
    {
        var window = GetWindow<GHSkillTool>("Texture Previewer");
        window.position = new Rect(0, 0, 400, 200);
        //preload texture
        //SkillStyleTool.Load();
        //
        window.Show();
    }
  
    [System.NonSerialized] private Vector2 mousePosition;
    private bool IsProSkin
    {
        get { return EditorGUIUtility.isProSkin; }
    }

    void OnGUI()
    {
        //if (SkillStyleTool.caretIcon == null)
        //{
        //    SkillStyleTool.Load();
        //}
        timeInfoEnd = EditorGUI.FloatField(new Rect(50, 250, 100, 100), timeInfoEnd);
        var e = Event.current;
        mousePosition = e.mousePosition;
      
        if (EditorApplication.isCompiling)
        {
            ShowNotification(new GUIContent ("Please\n ...Is Compiling..."));
            return;
        }

        if (e.type == EventType.ScrollWheel)
        {
            isScrollTimeline = true;
            e.Use();
        }
        else
        {
            isScrollTimeline = false;

        }
        DoScrubControls();
        GUI.skin = SkillStyleTool.skinSheet;
        EditorGUI.LabelField(new Rect(0, 0, 100, 100), "Van Ha", GUI.skin.GetStyle("label"));
        //length = EditorGUI.Slider(new Rect(0, 0, (widthRect + 62)*scale, 30), length, timeInfoStart, timeInfoEnd);
        //GUI.color = Color.white;
        GUI.color = Color.black;
        GUI.DrawTexture(new Rect(0, 10, widthRect * scale, 3), EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
        GUI.color = Color.black;
        GUI.DrawTexture(rectSlider, SkillStyleTool.caretIcon, ScaleMode.StretchToFill, true);

        //GUI.Box(new Rect(0, 0, 700 * length, 10), "");
        GUI.DrawTexture(new Rect(TimeToPos(length) - 1 , rectSlider.center.y, 2, 600), EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
        GUI.color = Color.white;
        // GUI.DrawTexture(new Rect( rectSlider.x, 0 , 2, 600), EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
        for (var i = timeInfoStart; i <= timeInfoEnd; i += timeInfoInterval)
        {
            var posX = TimeToPos(i);

            //Debug.Log("i = " + i);
            Rect markRect;
            if (i % 2 == 0)
            {
                GUI.color = EditorGUIUtility.isProSkin ? Color.blue : Color.black;
                markRect = Rect.MinMaxRect((posX ), heightMin, (posX  + 4), heightMin + 30);
                var text = string.Format("{0:00}", i);
                var size = GUI.skin.GetStyle("label").CalcSize(new GUIContent(text));
                var stampRect = new Rect(0, 0, size.x, size.y);
                stampRect.center = new Vector2(posX  + 4, heightMin - 10);
                GUI.color = Color.blue;
                GUI.Box(stampRect, text, (GUIStyle)"label");
                //  EditorGUI.DrawRect(stampRect, Color.cyan);
                GUI.color = Color.white;
            }
            else
            {
                GUI.color = EditorGUIUtility.isProSkin ? Color.blue : Color.black;
                markRect = Rect.MinMaxRect((posX ), heightMin + 10, (posX  + 4) , heightMin + 20);
                GUI.color = Color.white;
            }
            GUI.DrawTexture(markRect, EditorGUIUtility.whiteTexture);
            GUI.color = Color.white;

        }


      //  lengthRect.center = new Vector2(16, 16);
        GUI.color = IsProSkin ? Color.white : Color.black;
       // GUI.DrawTexture(lengthRect, SkillStyleTool.PlusTexture, ScaleMode.StretchToFill, true);
        GUI.color = Color.white;
    }
    float length = 0;
    float timeInfoStart = 0;
    float timeInfoEnd = 35;
    float timeInfoInterval = 1;
    float heightMin = 100f;
    float widthRect = 700f;
    float scale = 1f;
    bool isDragHandler;
    bool isScrollTimeline;
    void DoScrubControls()
    {
      //  Debug.Log(rectSlider.center);
        var e = Event.current;
        if (e.type == EventType.MouseDown && (new Rect(0, 10, widthRect * scale, 10)).Contains(mousePosition)
            && isDragHandler == false)
        {
            isDragHandler = true;
            updateHandler();
            e.Use();
        }
        if (e.type == EventType.MouseDrag && isDragHandler)
        {
            if (e.button == 0)
            {
                updateHandler();
            }
            e.Use();
        }
        if (e.type == EventType.MouseUp)
        {
            isDragHandler = false;
            e.Use();
        }

        if (isScrollTimeline)
        {
            if (e.delta.y < 0)
            {
                scale -= 0.01f;
            }
            else
            {
                scale += 0.01f;
            }
            float pos = TimeToPos(length);
            rectSlider = new Rect(pos-10, 5, 20, 20);

            //length = PosToTime(rectSlider.center.x);
        }
    }
    private void updateHandler()
    {
        float mousePos = Mathf.Clamp(mousePosition.x ,0f, widthRect * scale);
        rectSlider = new Rect(mousePos - 10, 5, 20, 20);
        //rectSlider.center = new Vector2(mousePosition.x, 15);
        length = PosToTime(rectSlider.x + 10);
    }
    float TimeToPos(float time)
    {
        float ratio = widthRect*scale / timeInfoEnd;
        float pos = time * ratio;
        return pos;
    }

    float PosToTime(float pos)
    {
        //Debug.Log("pos = " + pos);
        float ratio = widthRect*scale / timeInfoEnd;
        float time = pos / ratio;
        //Debug.Log("time = " + time);

        return time;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[InitializeOnLoad]
public class SkillStyleTool
{
    #region param
    const string PLUS_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABGdBTUEAALGPC/xhBQAAAAlwSFlzAAAOwwAADsMBx2+oZAAAA5lJREFUeF7tWktrFEEQXkFQIYKKEYKTru6enl1cycVDVBBz9QkGxaAgCJ6UIPojIqigwc3Rk3jxIgiCF0FQT568eBN8QR6iGPXiwVdVbw2Gzaw7szNx7dn+4GPCpqemv69reru6t+Lh4eHh4fFvYYwZjJQaNVKORVLupWsoxI5arbaem5QTIcBuo9SdUMoPRqpfMaP4b5Bv0YxGVQjFt5QHKGyKhFZ1aMWGIJeRPuf/f0STTvGt7gPF3ayRsATRSbRGKI3t4SyHcBdayvMkPkno30gmYMb8RBN2cSj3QJMdvveL7VK+EykL8PqYw7kHI8SZqso++ktpzRsOt3NIt4AT2TWa1JKEpWVEc4FQRzmkW9AAjbwZQAagkRMc0i2gATcKMuA4h3QL3gBvgDfAG+AN6GcDcBl8pQgD0Mj9HLK30FpviUCfxpr+qgbZoBFuRxQ/HQp43m0dEJPu10I+wFjTSc/5Q9kwUl/C5x7BGmQNd7k4GNAXkPO0tLXEke3EvOJjUhYkxV9G7Be1xSryhRLqEHc9P7ATzXq+IEErTWsCzR1CTbKE7oHpN9VNPd9r0mCRCfi6HmYp2aGUGo0kBnFk5FtJW3B4nQWADSwpG0Ih79K71RrYJdo9RlAXWVJ6VIeGNmsBn10d/Zh2LgD5hGWlRyTlTtfFE1nDbH1wcIClpQMdVpTIgIV6EGxiaenQPLEpjQHzI0JsZGnp0PcZQGd15TEA3uBX4VqWlg71en0AvwbnXDeBzxbus6xsMCBn8m5r95p2HdNtOY3Vn8AgX1zNgubow7OxSmU1S8oOA3DSHlQ6ZoKtA0AvRgDbWEr3QBPO4br6hwvLYhoou/yVak4P6z0sIT8oGAZ9iPxOGUGMy852LCprqKhJir+UcX+w/VesAG/R68tdLxZ0UBmBHq9KOUGTSzsaoY7RTg5Vk62CstCaKOV1itf6jKWk/kRKHVwx4d2guD1BfYBDugXaqyvCABphDukWvAHeAG+AN8Ab0OcGzBRkgJu/EQpBXc5bTpMBuQ43egnMgPFmSZosrhNpGaxBfvuvlrdZEATBOhTw0qZxgsBO5CO52xzOTWA9sM+mccbKkE17b7aagEO5i1CISVuupjSB234qtJ7vNewPF6R6FZ/hU0a0kj7nzYynKghG+NbygE5p7Q8tpHqEI/3OgFzAa5NCvTag7kWgT2DTVc07SgzadqfDCjqxIWbet/fw8PDwyItK5TfeK8SYMlvO4wAAAABJRU5ErkJggg==";

    const string MINUS_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAAEaEAABGhAVN0B3oAAAAZdEVYdFNvZnR3YXJlAHd3dy5pbmtzY2FwZS5vcmeb7jwaAAAAu0lEQVR4Xu3YIQoCURSF4dssbsBkFpPuQLDpDlyCMN1gttkMrsE1WcRkEgSNesYwjONdwTv/B3853PLqCwAAAAAAAAAAfA3VXC3VosDqd83UQP3oqaN6qrdBd7VTjZPKDktvr6JqDW69VNxag2Pp6FQ6OpWOTqWjU+noVDo6lY5OpaNT6ehUXDqDW7HqDE491NdBZQelt1GNrbqq7LC0zmqt/vTVWE3VpMDqd41U/QEEAAAAAAAAADAT8QETA0pfhJs/dQAAAABJRU5ErkJggg==";

    const string PLAY_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACxQAAAsUBidZ/7wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAOTSURBVHic5Zs7bFRHFIa/H0eYIkAFdDykVIBiaAAHKRUiWJRRBHGqSOFdgUGGFHQYlAIspIhEIUoV4iRKa2NCAwIMNLwNCCQenW0pUgwFRjInxWgU++7arHdn7sufNMXunTnnzqe5s2fv3cXMmNiANuAy8A/QC2xN9ilTS07+U2AMsET7G/g465ON0eYwma+BuVSyCbgl6SdJS6ocLyxJAS3v6fsN8FTSt5LmxTut9EgKSL6uxofAMeCxpHZJCn9a6VHLhKdiKfArcF3SxkDnkzqNCPCsA65I+kPSigDxUiWEAM8XwENJ30laGDBuVEIKAGgGDgFPJO2V9EHg+MEJLcCzCPgeuCOpLVKOIMQS4FkJ9Erql7Q6cq66iC3Asxm4LenHvBVSaQkAaAJ24vaHI3kppNIU4JkPdAGPJH2ZdSGVhQDPMuAcMCCpNauTyFKAZz1wTVKPpOVpJ8+DAM823GVxQtKCtJLmSQC4QqoT941zt6Sm2AnzJsCzCDiDK6S2xEyUVwGeVUCfpPOSVsVIkHcBns9wq+EHSYtDBi6KAHCF1C5cIdUpqTlE0CIJ8CwATuA+MbY1GqyIAjzLgR5J1yRtqDdIkQV4WnHV5G+Sls10cBkEeLbjLovjkubXPCrxYOQ2lQ9FitiGcN88m2b6ZKgsAny7B2yeyZOhsrEa6JfUK2lltQ5lF+BpA+5K6qg4UvJLoFprn02XQDU+n/hiNgqYxGwU8NfEF7NJwDhw0MzOTXwz94+uAtGHm/xg8kDZBdwHOszswlQdynoJDOPuHayZbvJQvhXwBugGuszsVS0DyiSgBzhsZi9mMqgMAgaAA2Z2vZ7BRd4DngPbzeyTeicPxVwBo7iHq91mNtZosCIJGAfOAkfNbDhU0KII6Md9nj8IHTjvAh7gKrjzsRLkdRMcAfYALTEnD/lbAWP8X8iMppEwTwJ+xxUyz9NMmgcBN4D9ZjaQRfIs94AXQDvQmtXkIZsV8Ao4DpwyszcZ5J9EmgLGgZ9xhcxQinmnJS0BF3CFzP2U8tVMbAGDuEKmL3Keuom1CY4A+3CFTG4nD+FXwBhwGjhmZv8Gjh2FkAL+BDrN7FnAmNEJIeAm7o7M1QCxUqeRPeAl8BWwoaiTh8oV8K6GMa9xhczJPBQyjZJcAXem6fsOd0fmIzPrKsPkoVLAL8DbKv0uAmvNbEeeqrggVPxoyP2a4hLu7/N9lPzv8/8BK7beEDtwWBcAAAAASUVORK5CYII=";
    const string PAUSE_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACxQAAAsUBidZ/7wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEESURBVHic7ZuxDcIwFAWPbJGeAdgsRRZkjzADZVrTuEI0WMk/JN5JX0pj+3xRygBcgAXYgHbg7MAduLXWGBng1vfYD3bb+p0v9IcjN3+fJzAPXH7ua890WzjhzX+adSDAWuC1TcCV8xk5o8RrKjgE+rdWsOZrqgL8LAlgC9gkgC1gkwC2gE0C2AI2CWAL2CSALWCTALaATQLYAjYJYAvYJIAtYJMAtoBNAtgCNglgC9gkgC1gkwC2gE0C2AI2CWAL2CSALWCTALaATQLYAjYJYAvYJIAtYJMAtoBNAtgCNglgC9gkgC1gkwC2gE1VgFa05msm4FFwzsgZZV5///P0X/8+/wK1BLImIdnmoQAAAABJRU5ErkJggg==";
    const string STOP_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAACxQAAAsUBidZ/7wAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAEESURBVHic7Zs7DsJAEMWcBeVAUO7p6HOG3CgFDZchKBQECSRacMQ+S9N7nE83LMvCc4AKjMAZuALLn8x13WkE6tvO6+I9MAC3Dch+e27rrv1rgGEDYr+eYd2d2siT//Qm1B1wAg60Rwd0BTjaJiLHjscfcm+bSMwdj++hWYotYJMAtoBNAtgCNglgC9gkgC1gkwC2gE0C2AI2CWAL2CSALWCTALaATQLYAjYJYAvYJIAtYJMAtoBNAtgCNglgC9gkgC1gkwC2gE0C2AI2CWAL2CSALWCTALaATQLYAjYJYAvYJIAtYFOA2ZYQmQtwsS1ELgWYbAuRCRo/nMzpbI6nGz+fvwPq5h/BJOGf/wAAAABJRU5ErkJggg==";
    const string FAST_FORWAR_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAAB+gAAAfoBF4pEbwAAABl0RVh0U29mdHdhcmUAd3d3Lmlua3NjYXBlLm9yZ5vuPBoAAAIZSURBVHic7do/iM9xHMfxx/tCcROXhEnd5bJIdsksZT+JzSJZJVksSpeVksEkdVkYRAaTZJTUuaLbMHCDPz8fg/tIxyXue32+ef/e9Vx+w6v379m37/fz5x2lFJlrpHUDrWsooHUDrWsooHUDrWsooHUDrWsoICK2RMSNiHgUEWciYrSr8IiYjIiZiHgQESciYk1X2V3WXZSfmMdxjJRSrAS8XJL9DAdXmtsl8HFJk5WnOLCC4B3L5Bbcw+7Wf74KWK7Jym3s/IfgyT/kDnAVW/suoOAzLmOsQwGV9ziL9X0WUHmH01jXoYDKKxxB9FlA5QUOdyyg8hj7+i6g8hB7OxZQuYXxvgso+Irr2N6xgOL7F+oSNvZZQGUB5zHaoYDKG5zE2j4LqMzjGHatQvZzHOq7gMrcKmbfx56+C1htBriGbVkFVD7gHDZkFVB5jaP+ciH1PwmoPMH+zAIqM5jILKDgE6axKauAyluc8puFVBYBlV82cdkEVH5s4mLxh4w1wFRmATCb/l4g8xMwwBTtX0hNX4LZBKT9DKZdCKVeCqfdDKXdDqc9EEl7JNb7Q9G5Vczu9bF42ouRtFdjqS9H016Ppx2QSDsik3pI6r8fk0s/KHlnSXNdjsrOLsnu3ahsYDMuYnxRxnQpZUEHFRETuIAx3MSVUsqXLrK7qqiPQdZKfy8wFNC6gdY1FNC6gdY1FNC6gdaVXsA3DKBq0mdzSjkAAAAASUVORK5CYII=";
    const string REFRESH_ICON = "iVBORw0KGgoAAAANSUhEUgAAAEAAAABACAYAAACqaXHeAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAAASPSURBVHhe7dtbqFVFHMfxU2ZaaSqiFhGJRkjeHroqQQX2UqQgkmmIFYhd1NSH0CwyRLCnii50wbxEPihKWl5LIbQ0ECJ7iDJNK/JBovs9q+/PzoJp8d9nz8yatU4P84MP55zt3rPW/F177VmzZnfk5OTkNJRBGIeZeBhP4+VOK/EEHsQ0XIH+SJ0BGPnvr81EHX4Mb+Nr/B3gBHZARRmFqjkH72Lh6b9qzEA8gIOwOhbjT+zBDPRGaNT5XVBbd+mBOnI+luArlDuQ0keYhTPhE7fzUksBpuJTuDtat/dwI7pKufOStAA63F+Fu4GmPY6zUM7Z2Iry85MV4Ep8jPIGuoNOshejiDr/Oqznzkbl3IofYG2guxzD5dC54Y3Oxyz3olKmQGdlq3Efv+Eo3sEWbOz8qb/1uP7dep2PL7C39FhZpQLcgpjOf4kXoeINgw5RK70wHJPxPI7Daq+K6AKMxY+wGm3lfdwJfUTGpC+m4wCs9mNEFaAfPoHVoOUk7oN1do6NBkEpjoioArwCqzHLTgxFHRmMDbC26yu4ADrjWw1ZdIHTRB7BL7D2oZ2gAmjcfRhWQ2XL0VR0VaeLJWs/2gkqwFxYjZQ9h6aiAc+HsPbDh3cBNI7W4MJqxKVR2BloIpeg6ujTuwC3w2rA9S20U03kUvi+HbtyD7yigYvVgGsBmshlOAJrH0J5Xwy9AKuBgi5/W43oUmYEPoe1DzG8C3AbrAYKmterO6Oh8b21/VjeBeiJ3bAa0XtRcwF15gb8Dmv7VXifAxQNgTVr+z304l+xGXWN9IroU0Wzw4uh80zhfqgDOpO3Mx/ua0UTolGzwhfhKuhKLicnJycnJycnJ+f/mQuhGeR5uBk9EJS7UazcSGU9JqHu3AFNybsXP7qLrJst3tHOug2koMUNWiJTZ66FtW3RHOK58IrW61iNxNLtc9/FDFWyDdb2C1rD4JW1sBqIsRpN5Gr8BWsfCivglVQLHl5CU9HMtLUPrkXwSooCPIumMgfWPrj+gPdESNUCPIWmokkazVJZ++EKeivGFkBV1nqdpqL7ET6zxZpX1LS6d2ILsB9NRXOSWi5n7UfZkwhKbAFUaW2s7ttk18Dnlp1oSj14yW3Vc8BbCDrkAqKbtT/D2q5FQ+HgVC2AfIeHoGn1FBmPN2FtqxXvz/1yUhSgoEP1Uej2Vmh0d1qLsjbBarsrWnUWnZQFKGjpmwYry6CLIq3nG4I+UEd1pOi+v0Z0xcWYlsxZbbWzD+chOl0V4BR0aKdYF/wNdJLSUaIF1j/Bel4Idb7y9wxaFUCdLy4odHn5AazndZfXoCOqcqwCuJ0vokpXXbGVSvQJz0q5AFbn3WjmRWd99zVN+QwTkTRuAdp1vog+9+uYSGlFgy5dc9Ryi34VtBHfzruZgO0o73AqWhu4BmNQW9ZBGwvtvBsNV59BqsXOh7AUdY0w/xNdOmohQororHwTtIhSq020alxHltXJgg5vfTRqikvfQdI3z4Jndqvkgs6fdUQF0f/i9dDSea0k15odfZdQS+SvgxZhxHwzLCcnJzQdHf8A1q8kIYOU9woAAAAASUVORK5CYII=";
    const string CARET_ICON = "CARET_ICON";
    #endregion
    public static bool DrawButtonIcon(Texture2D tex, Vector2 size)
    {
        var btIcon = new GUIContent(tex);
        return GUILayout.Button(btIcon, GUILayout.Width(size.x), GUILayout.Height(size.y));
    }

    public static Texture2D PlusTexture { get { return FromBase64(PLUS_ICON); } }
    public static Texture2D MinusTexutre { get { return FromBase64(MINUS_ICON); } }
    public static Texture2D PlayTexutre { get { return FromBase64(PLAY_ICON); } }
    public static Texture2D PauseTexutre { get { return FromBase64(PAUSE_ICON); } }
    public static Texture2D StopTexutre { get { return FromBase64(STOP_ICON); } }
    public static Texture2D FastForwarTexutre { get { return FromBase64(FAST_FORWAR_ICON); } }
    public static Texture2D RefreshTexture { get { return FromBase64(REFRESH_ICON); } }

    public static Texture2D FromBase64(string base64)
    {
        if (_iconDic.ContainsKey(base64))
        {
            return _iconDic[base64];
        }
        var tex = new Texture2D(64, 64, TextureFormat.RGBA32, false);
        tex.LoadImage(System.Convert.FromBase64String(base64), true);
        tex.hideFlags = HideFlags.HideAndDontSave;
        _iconDic.Add(base64, tex);
        return tex;
    }
    public static Dictionary<string, Texture2D> _iconDic = new Dictionary<string, Texture2D>();
    public static Texture2D caretIcon;
    public static Texture2D clipBox;
    public static GUISkin skinSheet;
    static SkillStyleTool()
    {
        Load();
    }

    [InitializeOnLoadMethod]
    public static void Load()
    {
        caretIcon = (Texture2D)Resources.Load("CarretIcon");
        clipBox = (Texture2D)Resources.Load("ClipBoxHorizontal");
        skinSheet = (GUISkin)Resources.Load("MyGuiSkin");
    }

    private static GUIStyle _labelSkill;
    public static GUIStyle labelSkill
    {
        get {
            if (_labelSkill == null)
            {
                _labelSkill = skinSheet.GetStyle("label");
            }
            return _labelSkill;
        }
    }
    //public static Color WithAlpha(this Color color, float alpha)
    //{
    //    color.a = alpha;
    //    return color;
    //}
}

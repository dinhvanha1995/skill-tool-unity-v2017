﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[ExecuteInEditMode]
public class TextEditor : MonoBehaviour {

    public Text textUI;
	// Use this for initialization
	void Start () {
        textUI = GetComponent<Text>();
    }
	
	// Update is called once per frame
	void Update () {
        GUIContent cont = new GUIContent("123AB");
        var text1 = "<b><i><size=30>" + "123" + "</size></i></b>";

        textUI.text = text1;
        Debug.Log(text1);
    }
}

Shader "SGame/Particles/Dissolve Alpha Blend"
{
  Properties
  {
    _color_T ("color_T", 2D) = "white" {}
    _color ("color", Color) = (1,1,1,1)
    _qiangdu ("qiangdu", float) = 1
    _dissulve_tex ("dissulve_tex", 2D) = "white" {}
    _dissuvle_vule ("dissuvle_vule", float) = 1
    _dissolve_edge_hard ("dissolve_edge_hard", Range(1, 100)) = 100
    _mask ("mask", 2D) = "white" {}
    _maskintensity ("maskintensity", float) = 1
    _Edge_color ("Edge_color", Color) = (1,1,1,1)
    _Edge_with ("Edge_with", Range(0, 1)) = 0.2
    _Edgeintensity ("Edgeintensity", Range(0, 10)) = 1
  }
  SubShader
  {
    Tags
    { 
      "IGNOREPROJECTOR" = "true"
      "QUEUE" = "Transparent+10"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "QUEUE" = "Transparent+10"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Blend SrcAlpha OneMinusSrcAlpha
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _color_T_ST;
      uniform float4 _dissulve_tex_ST;
      uniform float4 _mask_ST;
      uniform float4 _color;
      uniform float _dissuvle_vule;
      uniform float _Edge_with;
      uniform float4 _Edge_color;
      uniform float _Edgeintensity;
      uniform float _qiangdu;
      uniform float _maskintensity;
      uniform float _dissolve_edge_hard;
      uniform sampler2D _color_T;
      uniform sampler2D _dissulve_tex;
      uniform sampler2D _mask;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float2 texcoord :TEXCOORD0;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Vert
      {
          float2 texcoord :TEXCOORD0;
          float4 texcoord1 :TEXCOORD1;
          float4 color :COLOR0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float2 texcoord :TEXCOORD0;
          float4 texcoord1 :TEXCOORD1;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _color_T));
          out_v.texcoord1.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _dissulve_tex));
          out_v.texcoord1.zw = TRANSFORM_TEX(in_v.texcoord.xy, _mask);
          out_v.color = in_v.color;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat10_0;
      int u_xlatb0;
      float u_xlat16_1;
      float3 u_xlat16_2;
      float3 u_xlat10_3;
      float3 u_xlat16_4;
      float u_xlat16_7;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat10_0.xyz = tex2D(_dissulve_tex, in_f.texcoord1.xy).xyz.xyz;
          u_xlat16_1 = dot(u_xlat10_0.xyz, float3(0.300000012, 0.589999974, 0.109999999));
          u_xlat16_4.x = (((-_dissuvle_vule) * in_f.color.z) + 1);
          u_xlat16_7 = (u_xlat16_4.x + _Edge_with);
          if((u_xlat16_1>=u_xlat16_7))
          {
              u_xlatb0 = 1;
          }
          else
          {
              u_xlatb0 = 0;
          }
          u_xlat16_7 = (u_xlatb0)?((-1)):((-0));
          if((u_xlat16_1>=u_xlat16_4.x))
          {
              u_xlatb0 = 1;
          }
          else
          {
              u_xlatb0 = 0;
          }
          u_xlat16_1 = ((-u_xlat16_4.x) + u_xlat16_1);
          u_xlat16_1 = (u_xlat16_1 * _dissolve_edge_hard);
          u_xlat16_1 = clamp(u_xlat16_1, 0, 1);
          u_xlat16_4.x = (u_xlatb0)?(1):(0);
          u_xlat16_4.x = (u_xlat16_7 + u_xlat16_4.x);
          u_xlat16_4.xyz = float3((u_xlat16_4.xxx * _Edge_color.xyz));
          u_xlat16_4.xyz = float3((u_xlat16_4.xyz * _Edge_color.www));
          u_xlat16_4.xyz = float3((u_xlat16_4.xyz * float3(_Edgeintensity, _Edgeintensity, _Edgeintensity)));
          u_xlat10_0.xyz = tex2D(_color_T, in_f.texcoord.xy).xyz.xyz;
          u_xlat16_2.xyz = float3((u_xlat10_0.xyz * _color.xyz));
          out_f.color.xyz = float3(((u_xlat16_2.xyz * float3(float3(_qiangdu, _qiangdu, _qiangdu))) + u_xlat16_4.xyz));
          u_xlat10_3.xyz = tex2D(_mask, in_f.texcoord1.zw).xyz.xyz;
          u_xlat16_4.x = dot(u_xlat10_3.xyz, float3(0.300000012, 0.589999974, 0.109999999));
          u_xlat16_4.x = (u_xlat16_4.x * _maskintensity);
          u_xlat16_1 = (u_xlat16_4.x * u_xlat16_1);
          u_xlat16_1 = (u_xlat16_1 * in_f.color.w);
          u_xlat16_1 = (u_xlat16_1 * _color.w);
          out_f.color.w = (u_xlat10_0.x * u_xlat16_1);
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}

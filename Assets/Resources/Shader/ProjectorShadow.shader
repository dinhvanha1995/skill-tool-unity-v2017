﻿// Upgrade NOTE: replaced '_Projector' with 'unity_Projector'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "JTYL_Shader/ProjectorShadow"
{
	Properties 
	{
		_ShadowTex ("Base (RGB)", 2D) = "gray" {}
		_strength("Shadow Strength", Range(0,1)) = 0.5
	}

	SubShader 
	{
		Tags 
		{ 
			"Queue"="Transparent"
		}

		Pass
		{
			ZWrite Off
			Fog {Color (1, 1, 1)}
			AlphaTest Greater 0
			ColorMask RGB
			Blend DstColor Zero
			Offset -1, -1

			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct v2f
			{
				float4 pos:POSITION;
				float4 sproj:TEXCOORD0;
			};

			float4x4 unity_Projector;
			float _strength;
			sampler2D _ShadowTex;
			sampler2D _MaskTex;

			v2f vert(float4 vertex:POSITION)
			{
				v2f o;
				o.pos = UnityObjectToClipPos(vertex);
				o.sproj = mul(unity_Projector, vertex);

				return o;
			}

			float4 frag(v2f i):COLOR
			{
				float4 scolor = tex2Dproj(_ShadowTex, UNITY_PROJ_COORD(i.sproj));
				if (scolor.a > .5)
				{
					scolor = fixed4(_strength, _strength, _strength, 1);
				}
				else
				{
					scolor = fixed4(1, 1, 1, 1);
				}

				return scolor;
			}
			
			ENDCG

		}
	} 

	FallBack "Diffuse"
}

﻿Shader "Gihot/Character" 
{
	Properties 
	{   
        _NotVisibleColor ("NotVisibleColor (RGB)", Color) = (1,1,1,1)
        _MainTex ("Main Texure", 2D) = "white" {}
		[Header(Reflection)]
        _Illum ("Reflection Texture (G))", 2D) = "white" {}
        _SpecularColor ("Specular Color", Color) = (0.9191176,0.9191176,0.9191176,1)
        _Shininess ("Shininess", Range (0.03, 1)) = 0.078125
        _ShininessRange ("Shininess Range", Range (5, 200)) = 10
		[Header(Normal Map)]
        _Normal ("Normal Map", 2D) = "bump" {}
		_NormalIntensity("Normal Intensity", Range(0, 6)) = 1
		[Header(Ambient)]
		//[Toggle(ENABLE_AMBIENT)] _EnableAmbient("Enable Ambient", Int) = 1
		_AmbientColor("Ambient Color", Color) = (0.5,0.5,0.5,1)
		_AmbientPower("Ambient Power", Range(0,5)) = 3
		_BackLightDirXYZGlossW("边光方向(W是边光强度)", Vector) = (-1.3,0.3,1.5,0.7)
		[Header(Outline)]
		[Toggle(ENABLE_OUTLINE)] _EnableOutline("Enable Outline", Int) = 0
		_RimLightColor("Rim Light Color", Color) = (0.5,0.5,0.5,1)
		_RimPower("Rim Power", Range(0,1)) = 0.1

	}
	
	SubShader 
	{
		//Tags { "IgnoreProjector"="True" "Queue"="Transparent" "RenderType"="Transparent" }
		LOD 200

       Pass
		{
            Tags { "LightMode" = "ForwardBase"}
			Blend SrcAlpha OneMinusSrcAlpha
			ZTest Greater
			ZWrite Off
			Lighting off	
            SetTexture [_NoTex] { ConstantColor [_NotVisibleColor] combine constant * texture  }	
		}
		
        Pass
		{
			ZTest Less
			Lighting On
			Blend SrcAlpha OneMinusSrcAlpha          
		}
		CGPROGRAM
		#pragma surface surf PlayerShader 
		#pragma shader_feature ENABLE_OUTLINE
		#pragma shader_feature ENABLE_EMISSIVE
		//#pragma target 3.0

		sampler2D _MainTex;
		sampler2D _Normal;
		sampler2D _Illum;
		fixed4 _SpecularColor;
		fixed _Shininess;
		half _ShininessRange;
		half _NormalIntensity;
		half4 _BackLightDirXYZGlossW;

#if ENABLE_OUTLINE
		fixed4 _RimLightColor;
		half _RimPower;
#endif
		half _AmbientPower;
		fixed4 _AmbientColor;


		struct Input 
		{
			half2 uv_MainTex;
            half2 uv_Normal;
            half2 uv_Illum;
		};
		
	    inline fixed4 LightingPlayerShader (SurfaceOutput s, fixed3 lightDir, fixed3 viewDir, fixed atten)
        {
            viewDir = normalize ( viewDir );
            lightDir = normalize ( lightDir );
            fixed3 halfDirection = normalize ( lightDir + viewDir );			
			half NdotL = max(0, dot(  s.Normal, lightDir ));

            //float3 specularColor = _SpecularColor.rgb*s.IllumG*5.0;
            half3 directSpecular = _LightColor0.xyz * pow(max(0,dot(halfDirection,s.Normal)),_ShininessRange);
            half3 specular = directSpecular * s.Specular * _Shininess;

            float3 directDiffuse = NdotL * _LightColor0.rgb - UNITY_LIGHTMODEL_AMBIENT;
			directDiffuse += _AmbientColor.rgb * _AmbientPower;
            half3 diffuse = directDiffuse * s.Albedo;

            half normalDotView =1.0 - max(0,dot(s.Normal, viewDir));
			half3 emissive = dot(saturate(2.0 * _BackLightDirXYZGlossW.rgb * s.Normal), normalDotView);
#if ENABLE_OUTLINE
			emissive *= _RimLightColor.rgb;
#endif

            fixed4 c;
			c.rgb = diffuse + specular * _NormalIntensity;
#if ENABLE_OUTLINE
			c.rgb += emissive * _BackLightDirXYZGlossW.a * _RimPower;
#endif
            c.a = s.Alpha;
            return c;
        }		

		void surf (Input IN, inout SurfaceOutput o) 
		{
			fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
			fixed3 normals = UnpackNormal(tex2D(_Normal, IN.uv_Illum));
            fixed3 illum = tex2D(_Illum, IN.uv_MainTex);	
			
			o.Normal = normalize(normals);
			o.Albedo = c.rgb*0.2;
			o.Specular = _SpecularColor.rgb*illum.g*5.0;
            //o.IllumG = illum.g;
			o.Alpha = c.a;
		}
		ENDCG
	} 
	FallBack "Mobile/Diffuse"
    CustomEditor "ShaderForgeMaterialInspector"
}

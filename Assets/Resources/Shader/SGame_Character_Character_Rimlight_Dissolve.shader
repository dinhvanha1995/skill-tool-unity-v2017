Shader "SGame/Character/Character_Rimlight_Dissolve"
{
  Properties
  {
    _MainTex ("Base Texture", 2D) = "white" {}
    _Color ("Color", Color) = (0.5,0.5,0.5,1)
    _MainTexUSpeed ("MainTex U Speed", Range(-10, 10)) = 0
    _MainTexVSpeed ("MainTex V Speed", Range(-10, 10)) = 0
    _DistortTex ("Distort Texture", 2D) = "white" {}
    _DistortStrength ("Distort Strength", Range(0, 5)) = 1
    _DistortTexUSpeed ("DistortTex U Speed", Range(-10, 10)) = 0
    _DistortTexVSpeed ("DistortTex V Speed", Range(-10, 10)) = 0
    _MainTexStrength ("MainTex Strength", Range(0, 5)) = 0.8
    _OutlineColor ("Outline Color", Color) = (0.5,0.5,0.5,1)
    _FresnelStrength ("Fresnel Strength", Range(0, 5)) = 0.5
    _FresnelColorStrength ("Fresnel Color Strength", Range(0, 5)) = 1
    _DissolveTex ("Dissolve Texture", 2D) = "white" {}
    _DissolveAmount ("Dissolve Amount", Range(0, 1)) = 0.5
    _DissolveColorAmount ("Dissolve Color Amount", Range(0, 1)) = 0.1
    _DissolveColor ("Dissolve Color", Color) = (1,1,1,1)
    _DissolveColorStrength ("Dissolve Color Strength", Range(0, 5)) = 2
  }
  SubShader
  {
    Tags
    { 
      "QUEUE" = "AlphaTest"
      "RenderType" = "TransparentCutout"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "QUEUE" = "AlphaTest"
        "RenderType" = "TransparentCutout"
      }
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      #define conv_mxt4x4_0(mat4x4) float4(mat4x4[0].x,mat4x4[1].x,mat4x4[2].x,mat4x4[3].x)
      #define conv_mxt4x4_1(mat4x4) float4(mat4x4[0].y,mat4x4[1].y,mat4x4[2].y,mat4x4[3].y)
      #define conv_mxt4x4_2(mat4x4) float4(mat4x4[0].z,mat4x4[1].z,mat4x4[2].z,mat4x4[3].z)
      #define conv_mxt4x4_3(mat4x4) float4(mat4x4[0].w,mat4x4[1].w,mat4x4[2].w,mat4x4[3].w)
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float3 _WorldSpaceCameraPos;
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_WorldToObject;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _MainTex_ST;
      uniform float4 _DissolveTex_ST;
      //uniform float4 _Time;
      uniform float4 _Color;
      uniform float4 _OutlineColor;
      uniform float _MainTexStrength;
      uniform float _FresnelStrength;
      uniform float _FresnelColorStrength;
      uniform float _DissolveAmount;
      uniform float _DissolveColorAmount;
      uniform float4 _DissolveColor;
      uniform float _DissolveColorStrength;
      uniform float4 _DistortTex_ST;
      uniform float _DistortStrength;
      uniform float _MainTexUSpeed;
      uniform float _MainTexVSpeed;
      uniform float _DistortTexUSpeed;
      uniform float _DistortTexVSpeed;
      uniform sampler2D _DissolveTex;
      uniform sampler2D _DistortTex;
      uniform sampler2D _MainTex;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float4 normal :NORMAL0;
          float2 texcoord :TEXCOORD0;
      };
      
      struct OUT_Data_Vert
      {
          float4 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float3 texcoord2 :TEXCOORD2;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 texcoord :TEXCOORD0;
          float3 texcoord1 :TEXCOORD1;
          float3 texcoord2 :TEXCOORD2;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      float u_xlat6;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          u_xlat0 = (in_v.vertex.yyyy * conv_mxt4x4_1(unity_ObjectToWorld));
          u_xlat0 = ((conv_mxt4x4_0(unity_ObjectToWorld) * in_v.vertex.xxxx) + u_xlat0);
          u_xlat0 = ((conv_mxt4x4_2(unity_ObjectToWorld) * in_v.vertex.zzzz) + u_xlat0);
          u_xlat1 = (u_xlat0 + conv_mxt4x4_3(unity_ObjectToWorld));
          u_xlat0.xyz = float3(((conv_mxt4x4_3(unity_ObjectToWorld).xyz * in_v.vertex.www) + u_xlat0.xyz));
          u_xlat0.xyz = float3(((-u_xlat0.xyz) + _WorldSpaceCameraPos.xyz));
          out_v.texcoord1.xyz = float3(u_xlat0.xyz);
          out_v.vertex = mul(unity_MatrixVP, u_xlat1);
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _MainTex));
          out_v.texcoord.zw = TRANSFORM_TEX(in_v.texcoord.xy, _DissolveTex);
          u_xlat0.x = dot(in_v.normal.xyz, conv_mxt4x4_0(unity_WorldToObject).xyz);
          u_xlat0.y = dot(in_v.normal.xyz, conv_mxt4x4_1(unity_WorldToObject).xyz);
          u_xlat0.z = dot(in_v.normal.xyz, conv_mxt4x4_2(unity_WorldToObject).xyz);
          u_xlat0.xyz = float3(normalize(u_xlat0.xyz));
          out_v.texcoord2.xyz = float3(u_xlat0.xyz);
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float4 u_xlat0_d;
      float3 u_xlat16_0;
      float3 u_xlat10_0;
      int u_xlatb0;
      float u_xlat16_1;
      float3 u_xlat16_2;
      int u_xlatb3;
      float3 u_xlat16_4;
      float u_xlat16_7;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat10_0.x = tex2D(_DissolveTex, in_f.texcoord.zw).x;
          u_xlat16_1 = (u_xlat10_0.x + (-_DissolveAmount));
          if((u_xlat16_1<0))
          {
              u_xlatb0 = 1;
          }
          else
          {
              u_xlatb0 = 0;
          }
          if((_DissolveColorAmount>=u_xlat16_1))
          {
              u_xlatb3 = 1;
          }
          else
          {
              u_xlatb3 = 0;
          }
          u_xlat16_1 = (u_xlatb3)?(1):(0);
          if(((int(u_xlatb0) * (-1))!=0))
          {
              discard;
          }
          u_xlat16_4.x = dot(in_f.texcoord2.xyz, in_f.texcoord2.xyz);
          u_xlat16_4.x = rsqrt(u_xlat16_4.x);
          u_xlat16_4.xyz = float3((u_xlat16_4.xxx * in_f.texcoord2.xyz));
          u_xlat16_2.x = dot(in_f.texcoord1.xyz, in_f.texcoord1.xyz);
          u_xlat16_2.x = rsqrt(u_xlat16_2.x);
          u_xlat16_2.xyz = float3((u_xlat16_2.xxx * in_f.texcoord1.xyz));
          u_xlat16_4.x = dot(u_xlat16_4.xyz, u_xlat16_2.xyz);
          u_xlat16_4.x = clamp(u_xlat16_4.x, 0, 1);
          u_xlat16_4.x = ((-u_xlat16_4.x) + 1.00199997);
          u_xlat16_4.x = log2(u_xlat16_4.x);
          u_xlat16_7 = (_FresnelStrength * 3);
          u_xlat16_4.x = (u_xlat16_4.x * u_xlat16_7);
          u_xlat16_4.x = exp2(u_xlat16_4.x);
          u_xlat16_4.xyz = float3((u_xlat16_4.xxx * _OutlineColor.xyz));
          u_xlat0_d.xzw = (_Time.yyy * float3(_DistortTexUSpeed, _MainTexUSpeed, _MainTexVSpeed));
          u_xlat0_d.y = (_Time.y * _DistortTexVSpeed);
          u_xlat0_d = (u_xlat0_d + in_f.texcoord.xyxy);
          u_xlat0_d.xy = float2(TRANSFORM_TEX(u_xlat0_d.xy, _DistortTex));
          u_xlat10_0.x = tex2D(_DistortTex, u_xlat0_d.xy).x;
          u_xlat0_d.xy = float2(((u_xlat10_0.xx * float2(_DistortStrength, _DistortStrength)) + u_xlat0_d.zw));
          u_xlat10_0.xyz = tex2D(_MainTex, u_xlat0_d.xy).xyz.xyz;
          u_xlat16_0.xyz = float3((u_xlat10_0.xyz * _Color.xyz));
          u_xlat16_2.xyz = float3((u_xlat16_0.xyz * float3(_MainTexStrength, _MainTexStrength, _MainTexStrength)));
          u_xlat16_4.xyz = float3(((u_xlat16_4.xyz * float3(float3(_FresnelColorStrength, _FresnelColorStrength, _FresnelColorStrength))) + u_xlat16_2.xyz));
          u_xlat16_2.xyz = float3(((_DissolveColor.xyz * float3(_DissolveColorStrength, _DissolveColorStrength, _DissolveColorStrength)) + (-u_xlat16_4.xyz)));
          out_f.color.xyz = float3(((float3(u_xlat16_1, u_xlat16_1, u_xlat16_1) * u_xlat16_2.xyz) + u_xlat16_4.xyz));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack "SGame/Scene/SimpleTexture"
}

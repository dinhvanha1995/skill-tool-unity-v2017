Shader "SGame/Particles/Mask Add"
{
  Properties
  {
    _Texture ("Texture", 2D) = "white" {}
    _Color ("Color", Color) = (1,1,1,1)
    _Value ("Value", float) = 2
    _NiuQu ("NiuQu", 2D) = "white" {}
    _DUspeed ("DUspeed", float) = 0
    _DVsped ("DVsped", float) = 0
    _qiangdu ("qiangdu", float) = 0
    _Mask ("Mask", 2D) = "white" {}
    _U ("U", float) = 0
    _V ("V", float) = 0
    _Glow ("Glow", Range(0, 10)) = 1
  }
  SubShader
  {
 Tags
    {
      "IGNOREPROJECTOR" = "true"
      "QUEUE" = "Transparent+10"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "QUEUE" = "Transparent+10"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
       
      Blend One One, Zero One
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _Color;
      uniform float4 _Mask_ST;
      //uniform float4 _Time;
      uniform float4 _Texture_ST;
      uniform float4 _NiuQu_ST;
      uniform float _Value;
      uniform float _U;
      uniform float _V;
      uniform float _DUspeed;
      uniform float _DVsped;
      uniform float _qiangdu;
      uniform float _Glow;
      uniform sampler2D _NiuQu;
      uniform sampler2D _Texture;
      uniform sampler2D _Mask;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float2 texcoord :TEXCOORD0;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Vert
      {
          float4 texcoord :TEXCOORD0;
          float4 color :COLOR0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float4 texcoord :TEXCOORD0;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.texcoord.zw = TRANSFORM_TEX(in_v.texcoord.xy, _Mask);
          out_v.texcoord.xy = float2(in_v.texcoord.xy);
          out_v.color.xyz = float3((in_v.color.xyz * _Color.xyz));
          out_v.color.w = in_v.color.w;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float2 u_xlat0_d;
      float3 u_xlat10_0;
      float3 u_xlat16_1;
      float2 u_xlat2;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat0_d.xy = float2(((float2(_DUspeed, _DVsped) * _Time.yy) + in_f.texcoord.xy));
          u_xlat0_d.xy = float2(TRANSFORM_TEX(u_xlat0_d.xy, _NiuQu));
          u_xlat10_0.x = tex2D(_NiuQu, u_xlat0_d.xy).x;
          u_xlat2.xy = float2(((_Time.yy * float2(_U, _V)) + in_f.texcoord.xy));
          u_xlat0_d.xy = float2(((u_xlat10_0.xx * float2(float2(_qiangdu, _qiangdu))) + u_xlat2.xy));
          u_xlat0_d.xy = float2(TRANSFORM_TEX(u_xlat0_d.xy, _Texture));
          u_xlat10_0.xyz = tex2D(_Texture, u_xlat0_d.xy).xyz.xyz;
          u_xlat16_1.xyz = float3((u_xlat10_0.xyz * float3(_Value, _Value, _Value)));
          u_xlat16_1.xyz = float3((u_xlat16_1.xyz * in_f.color.xyz));
          u_xlat16_1.xyz = float3((u_xlat16_1.xyz * in_f.color.www));
          u_xlat10_0.xyz = tex2D(_Mask, in_f.texcoord.zw).xyz.xyz;
          u_xlat16_1.xyz = float3((u_xlat10_0.xyz * u_xlat16_1.xyz));
          out_f.color.xyz = float3((u_xlat16_1.xyz * float3(_Glow, _Glow, _Glow)));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}

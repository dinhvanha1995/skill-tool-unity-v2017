Shader "Gihot/TreeMotionLight"
{
	Properties
	{
		[Header(Textures)]
		_MainTex("Base (RGB) Trans (A)", 2D) = "white" {}
		_Cutoff( "Alpha cutoff", Range(0,1) ) = 0.5	

		[Header(Motion)]
		_MotionPowerWeightMask("MotionPowerWeightMask", 2D) = "white" {}
		_MotionSpeed("MotionSpeed", Range( 0 , 10)) = 1
		_MotionRange("MotionRange", Range( 0 , 10)) = 0.2
		[HideInInspector] _texcoord( "", 2D ) = "white" {}
		[HideInInspector] __dirty( "", Int ) = 1

	}

	SubShader
	{
		Tags{ "RenderType" = "TransparentCutout" }
			LOD 800
			//CGINCLUDE
			CGPROGRAM
			#pragma surface surf Lambert exclude_path:prepass vertex:vertexDataFunc 
			#pragma target 3.0

			struct Input {
				float2 uv_MainTex;
			};


			uniform float _MotionSpeed;
			uniform float _MotionRange;
			uniform sampler2D _MotionPowerWeightMask;
			uniform float _WorldSpaceOffset;

			uniform sampler2D _MainTex;
			uniform float _Cutoff;


			void vertexDataFunc(inout appdata_full v, out Input o)
			{
				UNITY_INITIALIZE_OUTPUT(Input, o);
				float mulTime44 = _Time.y * _MotionSpeed;
				float3 ase_worldPos = mul(unity_ObjectToWorld, v.vertex);
				float2 temp_cast_0 = (0.0).xx;
				float3 objToWorld31 = mul(unity_ObjectToWorld, float4(float3(0,0,0), 1)).xyz;
				float ifLocalVar6 = 0;
				UNITY_BRANCH
				if (frac(objToWorld31.x) <= 0.5)
					ifLocalVar6 = 0.5;
				else
					ifLocalVar6 = 1.0;
				float ifLocalVar10 = 0;
				UNITY_BRANCH
				if (frac(objToWorld31.z) <= 0.5)
					ifLocalVar10 = 0.5;
				else
					ifLocalVar10 = 1.0;

				float2 appendResult11 = (float2(ifLocalVar6 , ifLocalVar10));
				float2 break15 = lerp(temp_cast_0,appendResult11,1);
				float2 appendResult24 = (float2((break15.x + v.texcoord.xy.x + lerp(0.0,0.5,0)) , (break15.y + v.texcoord.xy.y + lerp(0.0,0.5,0))));
				float4 tex2DNode53 = tex2Dlod(_MotionPowerWeightMask, float4(appendResult24, 0, 0.0));
				v.vertex.xyz += ((sin((mulTime44 + (ase_worldPos.x + ase_worldPos.z))) * _MotionRange) * tex2DNode53.r * tex2DNode53.g * tex2DNode53.b);
			}
			void surf(Input i , inout SurfaceOutput o)
			{
				float4 c = tex2D(_MainTex, i.uv_MainTex);
				o.Albedo = c.rgb;
				clip(c.a - _Cutoff);
				o.Alpha = 1;
			}	
			ENDCG
		
	}

	SubShader{
	Tags { "RenderType" = "Opaque" }
	LOD 200
	CGPROGRAM
	#pragma surface surf Lambert noforwardadd

	sampler2D _MainTex;
	uniform float _Cutoff;

	struct Input {
		float2 uv_MainTex;
	};

	void surf(Input IN, inout SurfaceOutput o) {
		fixed4 c = tex2D(_MainTex, IN.uv_MainTex);
		o.Albedo = c.rgb;
		clip(c.a - _Cutoff);
		o.Alpha = c.a;

	}
	ENDCG
	}
	Fallback "Legacy Shaders/Transparent/Cutout/Diffuse"
}

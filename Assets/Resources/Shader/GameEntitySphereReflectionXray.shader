﻿#warning Upgrade NOTE: unity_Scale shader variable was removed; replaced 'unity_Scale.w' with '1.0'
// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'


//////////////////////////////////////////////////////////////////////////
//
//   FileName : GameEntitySphereReflection.shader
//     Author : Chiyer
// CreateTime : 2014-03-12
//       Desc :
//
//////////////////////////////////////////////////////////////////////////
Shader "Game-X/GameEntity/SphereReflectionXray"{
	Properties{
		_Color("Main Color", Color) = (1,1,1,1)
		_MainTex("Base (RGB)", 2D) = "white" {}
		_ReflectTex("Reflect", 2D) = "black" {}
		_ColorFactor("_Color Factor", Range(0.0, 10.0)) = 0.9
		_ReflectFactor("Reflect Factor", Range(0.0, 2.0)) = 1
		_ReflectPower("Reflect Power", Range(1.0, 10.0)) = 1
		_Cutoff("Alpha cutoff", Range(0,1)) = 0.5
		_Alpha("Alpha",Range(0,1)) = 0.4
		_BlinkTime("_BlinkTime",Range(0,1)) = 0
		_BlinkColor("Blink Color", Color) = (1,1,1,1)
		
		_AfterTex("_AfterTex", 2D) = "white" {}
		_AfterColor("After Color", Color) = (0.27, 0.29, 0.78,1)
	}
	SubShader{
		pass{
			Tags{ "LightMode" = "Vertex" }
			Blend One OneMinusSrcColor
			Cull Back
			Lighting Off
			ZWrite Off
			Ztest LEqual 

			CGPROGRAM
#pragma vertex vert    
#pragma fragment frag    
#pragma fragmentoption ARB_precision_hint_fastest
			sampler2D _AfterTex;
			float4 _AfterColor;
			fixed4 _Color;
			struct appdata{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
			};
			struct v2f{
				float4 pos : POSITION;
				float2 uv : TEXCOORD0;
			};
			v2f vert(appdata v){
				v2f o;
				o.pos = UnityObjectToClipPos(v.vertex);
				half2 capCoord;
				float3 nor = mul((float3x3)UNITY_MATRIX_IT_MV, v.normal);
				//capCoord.x = dot(UNITY_MATRIX_IT_MV[0].xyz,v.normal);  
				//capCoord.y = dot(UNITY_MATRIX_IT_MV[1].xyz,v.normal);  
				capCoord.x = nor.x;
				capCoord.y = nor.y;
				// The normal is not really defined in the model coordinate system, such as when the scale is not proportional
				// Need to use the inverse transpose matrix of the MV transformation matrix to turn the normal into the perspective coordinate system
				// Change the normal direction value range [-1, 1] to the texture uv range [0, 1]     
				o.uv = capCoord * 0.5 + 0.5;
				return o;
			}
			float4 frag(v2f i) : COLOR{
				float4 col = tex2D(_AfterTex, i.uv) * 2.0 * _AfterColor *  _Color.a;
				//col.a = _Color.a;
				return col;// *2.0 * _AfterColor;
			}
			ENDCG
		}
		Pass{
			Tags{
				"RenderType" = "Transparent"
				"Queue" = "Transparent"
			}
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM
#pragma vertex vert
#pragma fragment frag     
#include "UnityCG.cginc"
#pragma fragmentoption ARB_precision_hint_fastest
			struct vsIn{
				float4 vertex : POSITION;
				float3 normal : NORMAL;
				float2 texcoord : TEXCOORD0;
			};
			struct vsOut{
				float4  vertex : SV_POSITION;
				float2  uv : TEXCOORD0;
				float2  reflect : TEXCOORD1;
			};
			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			sampler2D _ReflectTex;
			float4 _ReflectTex_ST;
			fixed _ColorFactor;
			fixed _ReflectFactor;
			fixed _ReflectPower;
			fixed _Cutoff;
			fixed4 _BlinkColor;
			fixed _Alpha;
			fixed _BlinkTime;

			vsOut vert(vsIn v){
				vsOut o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				//model -> view ,so scale change it. so should resize the normal by the build-in variable : unity_Scale
				o.reflect = mul((float3x3)UNITY_MATRIX_MV, v.normal * 1.0).xy * 0.5 + 0.5;
				return o;
			}
			fixed4 frag(vsOut In) : COLOR{
				fixed4 c;
				fixed4 albedo = tex2D(_MainTex, In.uv) * _Color;
				//fixed3 ambient = fixed3(1,1,1) + (UNITY_LIGHTMODEL_AMBIENT.rgb - fixed3(0.25,0.25,0.25)) * 4;
				fixed3 ambient = fixed3(0.45, 0.45, 0.45);
				fixed3 ref = tex2D(_ReflectTex, In.reflect);

				if (_BlinkTime >0){
					c.rgb = albedo.rgb + _BlinkColor * _Alpha * _Color.a;
				}
				else{
					c.rgb = albedo.rgb * ambient * _ColorFactor + pow(ref, _ReflectPower) * _ReflectFactor *_Color.a;
				}
				//fixed3 color1 = albedo.rgb + _BlinkColor * _Alpha * _Color.a; // _BlinkTime=1
				//fixed3 color2 = albedo.rgb * ambient * _ColorFactor + pow(ref, _ReflectPower) * _ReflectFactor *_Color.a; //_BlinkTime=0
				//c.rgb = lerp(color2, color1, _BlinkTime*2);

				c.a = _Color.a;
				clip(c.a - _Cutoff);
				return c;
			}
			ENDCG
		}
	}
	FallBack "Diffuse"
}
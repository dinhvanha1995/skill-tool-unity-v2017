Shader "SGame/Particles/Dissolve Add"
{
  Properties
  {
    _Main_color ("Main_color", Color) = (1,1,1,1)
    _Value ("Value", float) = 1
    _color_T ("color_T", 2D) = "white" {}
    _dissulve_tex ("dissulve_tex", 2D) = "white" {}
    _dissuvle_vaule ("dissuvle_vaule", float) = 1
    _dissolve_edge_hard ("dissolve_edge_hard", Range(1, 100)) = 100
    _mask ("mask", 2D) = "white" {}
    _Edge_with ("Edge_with", Range(0, 1)) = 0.2
    _gaoguang ("gaoguang", Color) = (1,1,1,1)
    _gaoguang_qiangdu ("gaoguang_qiangdu", float) = 0
  }
  SubShader
  {
    Tags
    { 
      "IGNOREPROJECTOR" = "true"
      "QUEUE" = "Transparent+10"
      "RenderType" = "Transparent"
    }
    Pass // ind: 1, name: 
    {
      Tags
      { 
        "IGNOREPROJECTOR" = "true"
        "QUEUE" = "Transparent+10"
        "RenderType" = "Transparent"
      }
      ZWrite Off
      Cull Off
      Blend One One
      // m_ProgramMask = 6
      CGPROGRAM
      //#pragma target 4.0
      
      #pragma vertex vert
      #pragma fragment frag
      
      #include "UnityCG.cginc"
      
      
      #define CODE_BLOCK_VERTEX
      //uniform float4x4 unity_ObjectToWorld;
      //uniform float4x4 unity_MatrixVP;
      uniform float4 _color_T_ST;
      uniform float4 _dissulve_tex_ST;
      uniform float4 _mask_ST;
      uniform float _dissuvle_vaule;
      uniform float _Edge_with;
      uniform float4 _gaoguang;
      uniform float4 _Main_color;
      uniform float _gaoguang_qiangdu;
      uniform float _Value;
      uniform float _dissolve_edge_hard;
      uniform sampler2D _color_T;
      uniform sampler2D _dissulve_tex;
      uniform sampler2D _mask;
      struct appdata_t
      {
          float4 vertex :POSITION0;
          float2 texcoord :TEXCOORD0;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Vert
      {
          float2 texcoord :TEXCOORD0;
          float4 texcoord1 :TEXCOORD1;
          float4 color :COLOR0;
          float4 vertex :SV_POSITION;
      };
      
      struct v2f
      {
          float2 texcoord :TEXCOORD0;
          float4 texcoord1 :TEXCOORD1;
          float4 color :COLOR0;
      };
      
      struct OUT_Data_Frag
      {
          float4 color :SV_Target0;
      };
      
      float4 u_xlat0;
      float4 u_xlat1;
      OUT_Data_Vert vert(appdata_t in_v)
      {
          OUT_Data_Vert out_v;
          out_v.vertex = UnityObjectToClipPos(in_v.vertex);
          out_v.texcoord.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _color_T));
          out_v.texcoord1.xy = float2(TRANSFORM_TEX(in_v.texcoord.xy, _dissulve_tex));
          out_v.texcoord1.zw = TRANSFORM_TEX(in_v.texcoord.xy, _mask);
          out_v.color = in_v.color;
          return out_v;
      }
      
      #define CODE_BLOCK_FRAGMENT
      float3 u_xlat10_0;
      int u_xlatb0;
      float3 u_xlat16_1;
      float3 u_xlat16_2;
      float u_xlat16_5;
      float u_xlat16_10;
      OUT_Data_Frag frag(v2f in_f)
      {
          OUT_Data_Frag out_f;
          u_xlat10_0.xyz = tex2D(_color_T, in_f.texcoord.xy).xyz.xyz;
          u_xlat16_1.xyz = float3((u_xlat10_0.xyz * in_f.color.www));
          u_xlat16_1.xyz = float3((u_xlat16_1.xyz * _Main_color.xyz));
          u_xlat16_1.xyz = float3((u_xlat16_1.xyz * _Main_color.www));
          u_xlat10_0.xyz = tex2D(_dissulve_tex, in_f.texcoord1.xy).xyz.xyz;
          u_xlat16_10 = dot(u_xlat10_0.xyz, float3(0.300000012, 0.589999974, 0.109999999));
          u_xlat16_2.x = (((-_dissuvle_vaule) * in_f.color.z) + 1);
          u_xlat16_5 = (u_xlat16_2.x + _Edge_with);
          if((u_xlat16_10>=u_xlat16_5))
          {
              u_xlatb0 = 1;
          }
          else
          {
              u_xlatb0 = 0;
          }
          u_xlat16_5 = (u_xlatb0)?((-1)):((-0));
          if((u_xlat16_10>=u_xlat16_2.x))
          {
              u_xlatb0 = 1;
          }
          else
          {
              u_xlatb0 = 0;
          }
          u_xlat16_10 = (u_xlat16_10 + (-u_xlat16_2.x));
          u_xlat16_10 = (u_xlat16_10 * _dissolve_edge_hard);
          u_xlat16_10 = clamp(u_xlat16_10, 0, 1);
          u_xlat16_2.x = (u_xlatb0)?(1):(0);
          u_xlat16_2.x = (u_xlat16_5 + u_xlat16_2.x);
          u_xlat16_2.xyz = float3((u_xlat16_2.xxx * _gaoguang.xyz));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz * _gaoguang.www));
          u_xlat16_2.xyz = float3((u_xlat16_2.xyz * float3(_gaoguang_qiangdu, _gaoguang_qiangdu, _gaoguang_qiangdu)));
          u_xlat16_1.xyz = float3(((u_xlat16_1.xyz * float3(float3(_Value, _Value, _Value))) + u_xlat16_2.xyz));
          u_xlat16_1.xyz = float3((float3(u_xlat16_10, u_xlat16_10, u_xlat16_10) * u_xlat16_1.xyz));
          u_xlat10_0.xyz = tex2D(_mask, in_f.texcoord1.zw).xyz.xyz;
          out_f.color.xyz = float3((u_xlat10_0.xyz * u_xlat16_1.xyz));
          out_f.color.w = 1;
          return out_f;
      }
      
      
      ENDCG
      
    } // end phase
  }
  FallBack Off
}

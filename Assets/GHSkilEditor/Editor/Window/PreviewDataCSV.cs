﻿using GUIExtensions;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEditor.IMGUI.Controls;
using UnityEngine;

namespace GHTools.SkillCreate.Editor
{
    public class PreviewDataCSV : EditorWindow
    {
        public GHCreateSkillData gHCreateSkillData;
        private Vector2 scrollView;
        GUITableState tableState;
        private void OnEnable()
        {
            if (gHCreateSkillData == null)
            {
                gHCreateSkillData = GameObject.FindObjectOfType<GHCreateSkillData>();
            }
        }
        private void OnGUI()
        {
            if (gHCreateSkillData == null)
            {
                gHCreateSkillData = GameObject.FindObjectOfType<GHCreateSkillData>();
                if (gHCreateSkillData == null)
                {
                    Debug.Log("Not found GHCreateSkillData Component");
                    return;
                }
            }

            SerializedObject serializedObject = new SerializedObject(gHCreateSkillData);

            List<TableColumn> columns = new List<TableColumn>()
            {
                new TableColumn("stage", 60f, UtilLanguage.STAGEID),                                      // 1
                new TableColumn("id", 70f, UtilLanguage.SKILLID){enabledEntries = false, optional = true}, // 2
                new TableColumn("name", 130f, UtilLanguage.NAMESKILL) {optional = true},                      // 3
                new TableColumn("target_type", 80f),                                 // 4
                new TableColumn("cd", 60f, UtilLanguage.CD),                                          // 5
                new TableColumn("distance", 50f) {optional = true},                  // 6
                new TableColumn("max_distance", 80f) {optional = true},              // 7
                new TableColumn("sum_time", 60f) {optional = true},                 // 8
                new TableColumn("break_time", 110f) {optional = true},               // 9
                new TableColumn("action_id", 110f) {optional = true},                // 10
                new TableColumn("effect_id|bind", 130f, UtilLanguage.EFFECTID),                              // 11
                new TableColumn("stage_time", 80f) {optional = true},                // 12
                new TableColumn("attack_time", 80f) {optional = true},               // 13
                new TableColumn("max_num", 60f, UtilLanguage.MAXNUM) {optional = true},                  // 14
                new TableColumn("target_type", 80f) {optional = true},              // 15
                new TableColumn("range_type", 80f) {optional = true},               // 16
                new TableColumn("range", 80f),                                       // 17
                new TableColumn("dmg", 100f) {optional = true},                       // 18
                new TableColumn("knock_back_ID", 100f, UtilLanguage.KNOCK_BACK) {optional = true},             // 19
                new TableColumn("start_stage", 80f) {optional = true},              // 20
                new TableColumn("end_stage", 80f) {optional = true},                // 21
                new TableColumn("des_stage", 100f, UtilLanguage.DES_STAGE) {optional = true},                 // 22
                new TableColumn("note", 100f, UtilLanguage.DES_STAGE) {optional = true}                 // 23
            };
            List<List<TableEntry>> rows = new List<List<TableEntry>>();
            GHCreateSkillData targetObject = (GHCreateSkillData)serializedObject.targetObject;
            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();

            #region data of current skill
            string stateData = string.Empty;
            string particleData = string.Empty;
            string damageData = string.Empty;
            string funcData = string.Empty;
            string stageId = string.Empty;
            int idSkill = currentClipData.idSkill;
            string nameSkill = "\'" + currentClipData.nameSkill + "\'";
            string noteSkill = "\'" + currentClipData.noteSkill + "\'";
            int targetType = (int)currentClipData.targetType;
            float cd = currentClipData.countdown;
            float distance = currentClipData.distance;
            float max_distance = currentClipData.max_distance;
            float sum_time = currentClipData.sum_time;
            float break_time = currentClipData.break_time;
            string actionId = "\'" + currentClipData.Clip.name + "\'";
            int max_num = currentClipData.max_num;
            int range_type = (int)currentClipData.rangeType;
            string range = (currentClipData.range == null || string.IsNullOrEmpty(currentClipData.range)) ? "{}" : currentClipData.range;
            int knock_back_ID = (int)currentClipData.knock_back_ID;
            #endregion
            for (int i = 0; i < currentClipData.GHClipStages.Count; i++)
            {
                var stage = currentClipData.GHClipStages[i];
                stageId = "stage " + (i + 1).ToString();
                var listComponent = new List<GHSkillStateData>();
                int countDamage = 0;
                var stage_time = stage.LastTime - stage.FirstTime;
                float attack_time = Mathf.Infinity;
                float start_stage = stage.FirstTime;
                float last_stage = stage.LastTime;
                string des_stage = string.Empty;
                string damage_id = string.Empty;
                string effect_id = string.Empty;
                foreach (var objChild in currentClipData.GHSkillStateDatas)
                {
                    if (objChild.gameObject.name.Contains("DamageData_"))
                    {
                        DamageData damageCompo = objChild.GetComponent<DamageData>();
                        if (damageCompo != null && listComponent.Contains(damageCompo) == false)
                        {
                            if (damageCompo.AmountFirst >= stage.FirstTime && damageCompo.AmountFirst < stage.LastTime)
                            {
                                attack_time = Mathf.Min(attack_time, damageCompo.AmountFirst - stage.FirstTime);
                                damage_id += "\'" + damageCompo.damageFake.ToString() + "_" + ((damageCompo.AmountFirst - stage.FirstTime) * 1000).ToString() + "\'" + ",";
                                countDamage++;
                                listComponent.Add(damageCompo);
                            }
                            //damageData += (damageCompo.AmountFirst * 1000) + "_";

                        }
                    }
                    else if (objChild.gameObject.name.Contains("ParticleData_"))
                    {
                        ParticleData particleCompo = objChild.GetComponent<ParticleData>();
                        if (particleCompo != null && listComponent.Contains(particleCompo) == false)
                        {
                            if (particleCompo.AmountFirst >= stage.FirstTime && particleCompo.AmountFirst <= stage.LastTime)
                            {
                                effect_id += particleCompo.effectObject.name + "|" + particleCompo.bind.ToString() + ",";
                                listComponent.Add(particleCompo);
                            }
                            // particleData += (particleCompo.AmountFirst * 1000) + "_";
                        }
                    }
                    else if (objChild.gameObject.name.Contains("FunctionData_"))
                    {
                        FunctionData funcCompo = objChild.GetComponent<FunctionData>();
                        if (funcCompo != null && listComponent.Contains(funcCompo) == false)
                        {
                            if (funcCompo.AmountFirst >= stage.FirstTime && funcCompo.AmountFirst <= stage.LastTime)
                            {
                                if (des_stage == null || string.IsNullOrEmpty(des_stage))
                                {
                                    des_stage = funcCompo.des.Substring(0, 1).ToUpper() + funcCompo.des.Substring(1);
                                }
                                else
                                {
                                    des_stage += " Sau đó, " + funcCompo.des;
                                }
                                listComponent.Add(funcCompo);
                            }
                            // particleData += (particleCompo.AmountFirst * 1000) + "_";
                        }
                    }
                }
                if ((damage_id != null || string.IsNullOrEmpty(damage_id) == false) && damage_id.Contains("_"))
                {
                    damage_id = "{" + damage_id.Substring(0, damage_id.Length - 1) + "}";
                }
                if ((effect_id != null || string.IsNullOrEmpty(effect_id) == false) && effect_id.Contains("|"))
                {
                    effect_id = "\'" + effect_id.Substring(0, effect_id.Length - 1) + "\'";
                }
                if ((des_stage != null || string.IsNullOrEmpty(des_stage) == false))
                {
                    des_stage = "\'" + des_stage + "\'";
                }
                if (attack_time == Mathf.Infinity || attack_time > stage_time)
                {
                    attack_time = 0;
                }
                if (i == 0)
                {
                    rows.Add(new List<TableEntry>()
                     {
                    new LabelEntry (stageId),
                    new LabelEntry (idSkill.ToString()),
                    new LabelEntry (nameSkill),
                    new LabelEntry (targetType.ToString()),
                    new LabelEntry (cd.ToString()),
                    new LabelEntry (distance.ToString()),
                    new LabelEntry (max_distance.ToString()),
                    new LabelEntry (sum_time.ToString()),
                    new LabelEntry (break_time.ToString()),
                    new LabelEntry (actionId.ToString()),
                    new LabelEntry (effect_id),
                    new LabelEntry ((stage_time*1000).ToString()),
                    new LabelEntry ((attack_time * 1000).ToString()),
                    new LabelEntry (max_num.ToString()),
                    new LabelEntry (targetType.ToString()),
                    new LabelEntry (range_type.ToString()),
                    new LabelEntry (range.ToString()),
                    new LabelEntry (damage_id.ToString()),
                    new LabelEntry (knock_back_ID.ToString()),
                    new LabelEntry ((start_stage*1000).ToString()),
                    new LabelEntry ((last_stage*1000).ToString()),
                    new LabelEntry ((des_stage).ToString()),
                    new LabelEntry ((noteSkill).ToString()),
                    });
                }
                else
                {
                    rows.Add(new List<TableEntry>()
                     {
                    new LabelEntry (stageId),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (effect_id),
                    new LabelEntry ((stage_time*1000).ToString()),
                    new LabelEntry ((attack_time * 1000).ToString()),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (""),
                    new LabelEntry (damage_id.ToString()),
                    new LabelEntry (""),
                    new LabelEntry ((start_stage*1000).ToString()),
                    new LabelEntry ((last_stage*1000).ToString()),
                    new LabelEntry ((des_stage).ToString()),
                    new LabelEntry (""),
                    });
                }
              
            }
            tableState = GUITable.DrawTable(columns, rows, tableState);
        }
    }
    
}

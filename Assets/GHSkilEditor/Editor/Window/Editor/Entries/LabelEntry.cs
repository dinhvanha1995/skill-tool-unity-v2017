﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;


namespace GUIExtensions
{

	/// <summary>
	/// This entry class draws a string as a label.
	/// This is useful for properties you want to display in the table
	/// as read only, as the default PropertyField used in PropertyEntry uses editable fields.
	/// </summary>
	public class LabelEntry : TableEntry
	{

		string value;
		string toolTip;

		public override void DrawEntry (float width, float height)
		{
            GUI.skin.label.alignment = TextAnchor.MiddleCenter;
            if (toolTip != null)
            {
                EditorGUILayout.LabelField(new GUIContent(value, toolTip), GUI.skin.GetStyle("label"), GUILayout.Width(width), GUILayout.Height(height));
            }
            else
            {
                EditorGUILayout.LabelField(value, GUI.skin.GetStyle("label"), GUILayout.Width(width), GUILayout.Height(height));
            }
        }

		public override string comparingValue
		{
			get
			{
				return value;
			}
		}

		public LabelEntry (string value, string toolTip = null)
		{
			this.value = value;
			this.toolTip = toolTip;
		}
	}

}

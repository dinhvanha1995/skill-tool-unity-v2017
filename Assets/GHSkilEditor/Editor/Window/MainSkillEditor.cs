﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;
using UnityEditor.IMGUI.Controls;
using System;
using UnityEditor.SceneManagement;

namespace GHTools.SkillCreate.Editor
{
    public class MainSkillEditor
    {
        #region params
        public GHCreateSkillData gHCreateSkillData;
        public GHSkillEditor GHSkillEditor;
        public static List<GameObject> listDamage = new List<GameObject>();
        public static List<ParticlePlayer> listParticle = new List<ParticlePlayer>();
        private bool isFinishAnim = false;
        float timeSkillStart = 0;
        float timeSkillInterval = 0.5f;
        float heightMin = 60f;
        float widthRect = 700f;
        float scale = 1f;
        bool isDragHandler;
        bool isScrollTimeline;
        float timeCurrent = 0;
        float ratioInterval = 1f;
        float[] modulos = new float[] {1, 2, 4, 8, 16, 32, 64, 128 };
        float[] modulosInterval = new float[] {0.5f, 1, 2, 4, 8, 16, 32, 64};
        float deltaStage = 0.05f;
        bool isShowSetting = true;
        bool isShowConfig = true;
        bool isShowTimeline = true;
        TimeSpan timeCurr;
        Rect windowHeight;
        public Rect rectSlider = new Rect(-10, 5, 20, 20);

        [System.NonSerialized] private Vector2 mousePosition;
        private bool IsProSkin
        {
            get { return EditorGUIUtility.isProSkin; }
        }
        readonly int maxStage = 6;
      

        #endregion
      
        public MainSkillEditor()
        {
           // EditorApplication.update += UpdateParticle;
           // EditorApplication.update += UpdateDamage;
        }

        private void UpdateDamage()
        {
            if (listDamage.Count > 0)
            {
                foreach (var dame in listDamage)
                {
                    if (dame != null)
                    {
                        //dame.GetComponent<DamageText>().UpdateDamage();
                    }
                }
            }
        }

        private void UpdateParticle()
        {
            if (listParticle.Count > 0)
            {
                foreach (var ps in listParticle)
                {
                    if (ps != null)
                    {
                        //  ps.UpdateState();
                    }
                    else
                    {
                        listParticle.Remove(ps);
                    }
                }
            }
        }
       
        #region GUI render
        public void GUISkillEditor()
        {
            EditorStyles.boldLabel.alignment = TextAnchor.MiddleLeft;
            if (SkillStyleEditor.caretIcon == null)
            {
                SkillStyleEditor.Load();
            }
           // HanldeEvent();
            
            EditorGUILayout.Space();
            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(300));
                EditorGUILayout.LabelField("Source Data", EditorStyles.boldLabel);
                gHCreateSkillData = (GHCreateSkillData)EditorGUILayout.ObjectField(gHCreateSkillData, typeof(GHCreateSkillData), GUILayout.Width(200));

                if (gHCreateSkillData == null)
                {
                    if (GUILayout.Button("Find Or Create Skill Data", GUILayout.Width(200)))
                    {
                        //try find on scene
                        gHCreateSkillData = GameObject.FindObjectOfType<GHCreateSkillData>();
                        if (gHCreateSkillData == null)
                        {
                            gHCreateSkillData = new GameObject(UtilLanguage.NAME_GO).AddComponent<GHCreateSkillData>();
                        }
                    }
                    return;
                }
                EditorGUILayout.EndHorizontal();
            }
         //   isShowSetting = EditorGUILayout.ToggleLeft(new GUIContent("Show setting"), isShowSetting);
            isShowSetting = EditorGUILayout.Foldout(isShowSetting, new GUIContent("Show setting") );
            if (isShowSetting)
            {
                EditorGUILayout.BeginHorizontal();
                //Animation Tab
                {
                    EditorGUILayout.BeginVertical(GUILayout.Width(450));
                    //render gui setting
                    GUISetting();
                    //render gui animation
                    EditorGUILayout.EndVertical();
                }
                //Animation Clip Tab
                {
                    EditorGUILayout.BeginVertical(GUILayout.Width(350));

                    GUIAnimationClip();
                    EditorGUILayout.EndVertical();

                }
                EditorGUILayout.EndHorizontal();
            }

           
            {
                //gui animation timeline
                {
                    GUIAnimation();
                }
            }
        }
       
        #endregion

        #region GUI Setting On Scene
        private void GUISetting()
        {
            //header
            {
                EditorGUILayout.LabelField("Create Skill", EditorStyles.boldLabel);
                EditorGUILayout.BeginHorizontal();
                gHCreateSkillData.SkillTargetMode = (SkillTargetMode)EditorGUILayout.EnumPopup("Target mode", gHCreateSkillData.SkillTargetMode);
                EditorGUILayout.EndHorizontal();
            }
            EditorGUILayout.Space();
            //render charater
            {
                EditorGUILayout.LabelField("Environment", EditorStyles.boldLabel);
                {
                    if (gHCreateSkillData.Environment == null)
                    {
                        gHCreateSkillData.Environment = new ModelData();
                        gHCreateSkillData.Environment.Prefab = (GameObject)Resources.Load("Environment");
                    }
                    gHCreateSkillData.Environment.Prefab = (GameObject)EditorGUILayout.ObjectField(gHCreateSkillData.Environment.Prefab, typeof(GameObject));
                }
                EditorGUILayout.LabelField("Model", EditorStyles.boldLabel);
                EditorGUILayout.BeginHorizontal();
                {
                    //character
                    {
                        EditorGUILayout.BeginVertical();
                        EditorGUILayout.LabelField("Main Character");
                        if (gHCreateSkillData.CharacterData == null)
                        {
                            gHCreateSkillData.CharacterData = new ModelData();
                        }
                        gHCreateSkillData.CharacterData.Prefab = (GameObject)EditorGUILayout.ObjectField(gHCreateSkillData.CharacterData.Prefab, typeof(GameObject));
                        gHCreateSkillData.CharacterData.IsDefaultRotate = EditorGUILayout.Toggle("Default Rotate?", gHCreateSkillData.CharacterData.IsDefaultRotate);
                        EditorGUI.BeginDisabledGroup(gHCreateSkillData.CharacterData.IsDefaultRotate);
                        gHCreateSkillData.CharacterData.CustomRotate = EditorGUILayout.Vector3Field("Custom Rotate", gHCreateSkillData.CharacterData.CustomRotate);
                        EditorGUI.EndDisabledGroup();
                        EditorGUILayout.EndVertical();
                    }
                    if (gHCreateSkillData.SkillTargetMode == SkillTargetMode.EnemyInteraction)
                    {
                        //target
                        {
                            EditorGUILayout.BeginVertical();
                            EditorGUILayout.LabelField("Main Target");
                            if (gHCreateSkillData.TargetData == null)
                            {
                                gHCreateSkillData.TargetData = new ModelData();
                            }
                            gHCreateSkillData.TargetData.Prefab = (GameObject)EditorGUILayout.ObjectField(gHCreateSkillData.TargetData.Prefab, typeof(GameObject));
                            gHCreateSkillData.TargetData.IsDefaultRotate = EditorGUILayout.Toggle("Default Rotate?", gHCreateSkillData.TargetData.IsDefaultRotate);
                            EditorGUI.BeginDisabledGroup(gHCreateSkillData.TargetData.IsDefaultRotate);
                            gHCreateSkillData.TargetData.CustomRotate = EditorGUILayout.Vector3Field("Custom Rotate", gHCreateSkillData.TargetData.CustomRotate);
                            EditorGUI.EndDisabledGroup();
                            EditorGUILayout.EndVertical();
                        }
                    }
                    else
                    {
                        EditorGUILayout.LabelField("No Target");
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
            //render setting in scene
            {
                EditorGUILayout.LabelField("Setting Scene", EditorStyles.boldLabel);

                if (gHCreateSkillData.SkillTargetMode == SkillTargetMode.EnemyInteraction)
                {
                    gHCreateSkillData.DistanceCharacterVsTarget = EditorGUILayout.FloatField("Distance Char vs Target", gHCreateSkillData.DistanceCharacterVsTarget);
                }
                //gHCreateSkillData.CameraViewMode = (CameraViewMode)EditorGUILayout.EnumPopup("Camera View Mode", gHCreateSkillData.CameraViewMode);
                {
                    EditorGUILayout.LabelField("Setting Command", EditorStyles.boldLabel);
                    EditorGUILayout.BeginHorizontal();
                    if (GUILayout.Button("Refresh Scene", GUILayout.Width(110)))
                    {
                        gHCreateSkillData.RefreshScene();

                        //environment
                        gHCreateSkillData.Environment.CheckOrCreateModel(UtilLanguage.ENVIRONMENT);
                    }
                    if (GUILayout.Button("Refresh Camera", GUILayout.Width(110)))
                    {
                        refreshCamera();
                    }
                    if (GUILayout.Button("Reset Scene", GUILayout.Width(110)))
                    {
                        gHCreateSkillData.CharacterData.Reset();
                        gHCreateSkillData.TargetData.Reset();
                    }
                    EditorGUILayout.EndHorizontal();
                }
            }
        }

        #endregion

        #region Animation Clip Tab
        private string currentClipLog;
        private Vector2 amountScrollAnimationClip;
        private void GUIAnimationClip()
        {
           
            if (gHCreateSkillData.AnimationClips == null)
            {
                gHCreateSkillData.AnimationClips = new List<AnimationClip>();
            }

            {
                EditorGUILayout.BeginHorizontal(GUILayout.Width(100));
                EditorGUILayout.LabelField("Animation Clip", EditorStyles.boldLabel);

                if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.PlusTexture, new Vector2(20, 20)))
                {
                    gHCreateSkillData.AnimationClips.Add(null);
                }
                if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.MinusTexutre, new Vector2(20, 20)))
                {
                    if (gHCreateSkillData.AnimationClips.Count > 0)
                    {
                        gHCreateSkillData.AnimationClips.RemoveAt(gHCreateSkillData.AnimationClips.Count - 1);
                    }
                }
                EditorGUILayout.EndHorizontal();
            }
           

            //render clip animation
            {
                amountScrollAnimationClip = EditorGUILayout.BeginScrollView(amountScrollAnimationClip, false, false, GUILayout.Width(350), GUILayout.Height(200));

                for (int i = 0; i < gHCreateSkillData.AnimationClips.Count;)
                {
                    EditorGUILayout.BeginHorizontal();
                    var clip = gHCreateSkillData.AnimationClips[i];
                    var clipName = clip != null ? i + "." + clip.name : i + ".Missing Clip";
                    gHCreateSkillData.AnimationClips[i] = (AnimationClip)EditorGUILayout.ObjectField(clipName, clip, typeof(AnimationClip), GUILayout.Width(250));
                    if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.MinusTexutre, new Vector2(15, 15)))
                    {
                        gHCreateSkillData.AnimationClips.RemoveAt(i);
                    }
                    else
                    {
                        i++;
                    }
                    EditorGUILayout.EndHorizontal();
                }
                EditorGUILayout.EndScrollView();
                //show log
                EditorGUILayout.Space();
                EditorGUILayout.LabelField(currentClipLog);
               
                if (GUILayout.Button("Update Animation List", GUILayout.Width(150)))
                {
                    if (gHCreateSkillData.GHAnimClipDataList == null)
                    {
                        gHCreateSkillData.GHAnimClipDataList = new List<GHAnimClipData>();
                    }
                    //get animation of model
                    if (gHCreateSkillData.CharacterData.Model != null)
                    {
                        //add new component
                        foreach (var clip in gHCreateSkillData.AnimationClips)
                        {
                            if (clip != null && gHCreateSkillData.GHAnimClipDataList.Exists(r => r.Clip == clip) == false)
                            {
                                var clipData = new GHAnimClipData(clip);
                                gHCreateSkillData.GHAnimClipDataList.Add(clipData);
                            }
                        }

                        //foreach (var clipData in gHCreateSkillData.GHAnimClipDataList)
                        //{
                        //    if (clipData != null && gHCreateSkillData.AnimationClips.Contains<AnimationClip>(clipData.Clip) == false)
                        //    {
                        //        gHCreateSkillData.GHAnimClipDataList.Remove(clipData);
                        //    }
                        //}
                        for (int i = 0; i < gHCreateSkillData.GHAnimClipDataList.Count;)
                        {
                            var clipData = gHCreateSkillData.GHAnimClipDataList[i];
                            if (clipData != null && gHCreateSkillData.AnimationClips.Contains<AnimationClip>(clipData.Clip) == false)
                            {
                                foreach (var gameObj in clipData.GHSkillStateDatas)
                                {
                                    if (gameObj != null)
                                    {
                                        GameObject.DestroyImmediate(gameObj);
                                    }
                                }
                                gHCreateSkillData.GHAnimClipDataList.Remove(clipData);
                            }
                            else
                            {
                                i++;
                            }
                        }
                        currentClipLog = "Update Animation List Completed!!!";

                    }
                    else
                    {
                        currentClipLog = "Character Model is null";
                    }
                }
            }

        }
        private static AnimationClip getAnimationClip(UnityEngine.Object[] objects, string clipName)
        {
            foreach (UnityEngine.Object obj in objects)
            {
                AnimationClip clip = obj as AnimationClip;
                if (clip != null && clip.name == clipName)
                {
                    return clip;
                }
            }
            return null;

        }
        #endregion
         

        #region Setup Camera
        private void refreshCamera()
        {
            switch (gHCreateSkillData.CameraViewMode)
            {
                case CameraViewMode.InFront:

                    break;
                case CameraViewMode.Behind:

                    break;
                case CameraViewMode.Left:

                    break;
                case CameraViewMode.Right:

                    break;
            }
        }
        #endregion

        #region Player Animation 
        public bool isPlay = false;

        private void OnPressPlay()
        {
            AnimationMode.StartAnimationMode();
            isPlay = true;
            isPlayParticle = true;
        }
        private void OnPressPause()
        {
            isPlay = false;
        }
        private void OnPressStop()
        {
            isPlay = false;
            timeCurrent = 0;
            rectSlider = new Rect(-10, 5, 20, 20);
            gHCreateSkillData.RefreshScene();
            GHSkillEditor.Repaint();
        }
        
        void DeleteGo(GameObject go)
        {
            if (Application.isEditor)
            {
                GameObject.DestroyImmediate(go);
            }
            else
            {
                GameObject.Destroy(go);
            }
        }

        void PlayDamageOrParticle()
        {
            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();
            foreach (var dataObj in currentClipData.GHSkillStateDatas)
            {
                var item = dataObj.GetComponent<GHSkillStateData>();

                if (timeCurrent >= item.AmountFirst && !item.isPlayed && isPlay)
                {
                    item.ExcuteState();
                    item.isPlayed = true;
                }
                else
                {
                    if (timeCurrent == 0)
                    {
                        item.isPlayed = false;
                        //gHCreateSkillData.RefreshScene();
                    }
                }
            }
        }

        private void OnPressNextFrame()
        {
            isPlay = false;
            if (gHCreateSkillData == null) return;

            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();
            if (currentClipData == null) return;
            if (gHCreateSkillData.CharacterData.Model == null) return;

            //if (gHCreateSkillData.MainAnimationSlider < 1)
            //{
            //    //Debug.Log(" add " + Time.deltaTime / (currentClipData.Clip.length));
            //    gHCreateSkillData.MainAnimationSlider += Time.deltaTime / (currentClipData.Clip.length);
            //    Mathf.Clamp(gHCreateSkillData.MainAnimationSlider, 0f, 1f);
            //}
            if (timeCurrent <= currentClipData.timeSkillEnd)
            {
                //Debug.Log(" add " + Time.deltaTime / (currentClipData.Clip.length));
                rectSlider.x += (Time.deltaTime * 20);
                timeCurrent = PosToTime(rectSlider.x + 10);
                Mathf.Clamp(timeCurrent, 0f, currentClipData.timeSkillEnd);
            }

        }
        public void Update()
        {
            if (gHCreateSkillData == null) return;

            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();
            if (currentClipData == null) return;
            if (gHCreateSkillData.CharacterData.Model == null) return;

            if (isPlay)
            {
                if (timeCurrent < currentClipData.timeSkillEnd)
                {
                    //  Debug.Log(" add " + Time.deltaTime / (currentClipData.Clip.length));
                    timeCurrent += Time.deltaTime;
                    Mathf.Clamp(timeCurrent, 0f, currentClipData.timeSkillEnd);
                   
                    isFinishAnim = false;
                }
                else
                {
                    // isPlay = false;
                    OnPressStop();
                    isFinishAnim = true;
                    isPlayParticle = false;
                }

            }
            // There is a bug in AnimationMode.SampleAnimationClip which crashes
            // Unity if there is no valid controller attached
            var go = gHCreateSkillData.CharacterData.Model.GetChild(0).gameObject;

           // Animator animator = go.GetComponent<Animator>();
           // if (animator != null && animator.runtimeAnimatorController == null)
             //   return;

            // Animate the GameObject
            //if (!EditorApplication.isPlaying && AnimationMode.InAnimationMode())
            if (AnimationMode.InAnimationMode())
            {
                AnimationMode.BeginSampling();
                float delayTime = currentClipData.AnimStages.FirstTime - timeCurrent;
                if (delayTime <= 0)
                {
                    float timeExecuteAnim = Mathf.Clamp(Mathf.Abs(delayTime), 0, currentClipData.AnimStages.LastTime);
                    AnimationMode.SampleAnimationClip(go, currentClipData.Clip, timeExecuteAnim);
                }
               
                PlayDamageOrParticle();
                AnimationMode.EndSampling();
                
                SceneView.RepaintAll();
            }
            if (isFinishAnim && gHCreateSkillData.isLoop)
            {
                OnPressPlay();
            }
        }
        #endregion
        float minLimitSecond = 0;
        #region GUI Show Animation
        private Vector2 scrollViewPos;
        private bool isPlayParticle = false;
        private void GUIAnimation()
        {
            if (gHCreateSkillData.GHAnimClipDataList == null)
            {
                gHCreateSkillData.GHAnimClipDataList = new List<GHAnimClipData>();
                gHCreateSkillData.IndexClipSelected = 0;
            }
           
            if (gHCreateSkillData.GHAnimClipDataList.Count == 0)
            {
                return;
            }
           
            EditorGUILayout.LabelField("Animation Player", EditorStyles.boldLabel);
            {
                //GUILayout.BeginHorizontal();
                //EditorGUI.BeginChangeCheck();
                //GUILayout.Toggle(AnimationMode.InAnimationMode(), "Animate");
                //if (EditorGUI.EndChangeCheck())
                //    ToggleAnimationMode();

                //GUILayout.FlexibleSpace();
                //GUILayout.EndHorizontal();
            }

            //render menu control
            {
                EditorGUILayout.BeginHorizontal();
                //slider
                if (isPlay == false)
                {
                    if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.PlayTexutre, new Vector2(20, 20), SkillStyleEditor.playButton))
                    {
                        OnPressPlay();
                    }
                }
                else
                {
                    if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.PauseTexutre, new Vector2(20, 20), SkillStyleEditor.pauseButton))
                    {
                        OnPressPause();
                    }
                }

                if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.StopTexutre, new Vector2(20, 20), SkillStyleEditor.stopButton))
                {
                    OnPressStop();
                }
                if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.FastForwarTexutre, new Vector2(20, 20), SkillStyleEditor.nextFrame))
                {
                    OnPressNextFrame();
                }
                gHCreateSkillData.isLoop = GUILayout.Toggle(gHCreateSkillData.isLoop, "Loop");
                EditorGUILayout.EndHorizontal();
            }

            //render select clip
            EditorGUILayout.BeginHorizontal(GUILayout.Width(150));
            {
                EditorGUILayout.LabelField("Select Animation", EditorStyles.boldLabel, GUILayout.Width(150));
                if (gHCreateSkillData != null)
                {
                    string[] anumClip = new string[gHCreateSkillData.GHAnimClipDataList.Count];
                    for (int i = 0; i < gHCreateSkillData.GHAnimClipDataList.Count; i++)
                    {
                        if (gHCreateSkillData.GHAnimClipDataList[i] != null)
                        {
                            anumClip[i] = gHCreateSkillData.GHAnimClipDataList[i].Name;
                        }
                    }
                    gHCreateSkillData.IndexClipSelected = EditorGUILayout.Popup(gHCreateSkillData.IndexClipSelected, anumClip, GUILayout.Width(150));
                }

                //render state skill button 
                {
                    EditorGUILayout.BeginHorizontal();
                    // gHCreateSkillData.CreateSkillElementType = (SkillElementType)EditorGUILayout.EnumPopup(gHCreateSkillData.CreateSkillElementType, GUILayout.Width(70));
                    //EditorGUILayout.LabelField("Create State Skill", EditorStyles.boldLabel);
                    if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.PlusTexture, new Vector2(20, 20)))
                    {
                        GenericMenu menu = new GenericMenu();
                        List< SkillElementType> listType = Enum.GetValues(typeof(SkillElementType)).Cast<SkillElementType>().ToList();
                        foreach (var item in listType)
                        {
                            AddMenuItemForColor(menu, item.ToString(), item);
                            menu.AddSeparator("");
                        }
                    
                        // display the menu
                        menu.ShowAsContext();
                    }
                    EditorGUILayout.EndHorizontal();
                }

            }
            EditorGUILayout.EndHorizontal();
            // render stage skill
            {
                GUIConfigSkill();
            }

            isShowTimeline = EditorGUILayout.Foldout(isShowTimeline, new GUIContent("Show timeline"));
            if (isShowTimeline)
            {
                EditorGUILayout.BeginVertical(GUILayout.Height(420));
                GUIStageAnimationClip();
                EditorGUILayout.EndHorizontal();
            }

            //render save data
            {
                GUIButton();
            }
            if (!EditorApplication.isPlaying)
            {
                EditorSceneManager.SaveOpenScenes();
                AssetDatabase.SaveAssets();
            }
        }

        private void GUIButton()
        {
            EditorGUILayout.BeginHorizontal();
            if (string.IsNullOrEmpty(GHCreateSkillData.PathExportCSV))
            {
                GHCreateSkillData.PathExportCSV = Application.dataPath;
            }
            if (string.IsNullOrEmpty(GHCreateSkillData.PathExportPrefab))
            {
                GHCreateSkillData.PathExportPrefab = Application.dataPath;
            }
            if (GUILayout.Button("Preview Data CSV", GUILayout.Width(150)))
            {
                windowPreviewCSV = EditorWindow.GetWindow<PreviewDataCSV>();
                windowPreviewCSV.titleContent = new GUIContent("Preview Data", "Xuất data mẫu cho action hiện tại.");
                windowPreviewCSV.minSize = new Vector2(800, 300);
                windowPreviewCSV.maxSize = new Vector2(1200, 400);
                windowPreviewCSV.Show();
            }
            if (GUILayout.Button("Save CSV", GUILayout.Width(150)))
            {
                string newPath = EditorUtility.SaveFolderPanel("Select Folder Save CSV ", GHCreateSkillData.PathExportCSV, "");
                if (!string.IsNullOrEmpty(newPath))
                {
                    if (GHCreateSkillData.PathExportCSV != newPath)
                    {
                        GHCreateSkillData.PathExportCSV = newPath;
                        EditorPrefs.SetString(GHCreateSkillData.OUT_PATH_CSV, GHCreateSkillData.PathExportCSV);
                    }
                }
                if (newPath.Length > 0)
                {
                    string nameFile = gHCreateSkillData.CharacterData.Prefab.name + "_SkillConfig";
                    gHCreateSkillData.ExportCSVFile(nameFile);
                }
            }
            if (GUILayout.Button("Save Prefab", GUILayout.Width(150)))
            {
                string newPath = EditorUtility.SaveFolderPanel("Select Folder Save Prefab ", GHCreateSkillData.PathExportPrefab, "");
                if (!string.IsNullOrEmpty(newPath))
                {
                    if (GHCreateSkillData.PathExportPrefab != newPath)
                    {
                        GHCreateSkillData.PathExportPrefab = newPath;
                        EditorPrefs.SetString(GHCreateSkillData.OUT_PATH_PREFAB, GHCreateSkillData.PathExportPrefab);
                    }
                }

                if (newPath.Length > 0)
                {
                    if (newPath.Contains("" + Application.dataPath))
                    {
                        string s = "" + newPath + "/";
                        string d = "" + Application.dataPath + "/";
                        string p = "Assets/" + s.Remove(0, d.Length);
                        GHCreateSkillData[] objs = GameObject.FindObjectsOfType<GHCreateSkillData>();
                        bool cancel = false;
                        foreach (GHCreateSkillData data in objs)
                        {
                            if (!cancel)
                            {
                                if (AssetDatabase.LoadAssetAtPath(p + data.gameObject.name + ".prefab", typeof(GameObject)) != null)
                                {
                                    int option = EditorUtility.DisplayDialogComplex("Are you sure?", "" + data.gameObject.name + ".prefab" + " already exists. Do you want to overwrite it?", "Yes", "No", "Cancel");
                                    switch (option)
                                    {
                                        case 0:
                                            CreateNewPrefab(data.gameObject, p + data.gameObject.name + ".prefab");
                                            goto case 1;
                                        case 1:
                                            break;
                                        case 2:
                                            cancel = true;
                                            break;
                                        default:
                                            Debug.LogError("Unrecognized option.");
                                            break;
                                    }
                                }
                                else CreateNewPrefab(data.gameObject, p + data.gameObject.name + ".prefab");
                            }
                        }
                    }
                    else
                    {
                        Debug.LogError("Prefab Save Failed: Can't save outside project: " + newPath);
                    }
                }

            }
            if (GUILayout.Button("Close", GUILayout.Width(150)))
            {
                GHSkillEditor.Close();

            }
            EditorGUILayout.EndHorizontal();
        }

        private void GUIConfigSkill()
        {
            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();


            if (currentClipData == null) return;
            //check current Stage
            if (currentClipData.GHClipStages == null)
            {
                currentClipData.GHClipStages = new List<GHClipStage>();
                currentClipData.AddNextStage();
            }

            scale = EditorGUILayout.Slider(scale, 0.1f, 10f);
            if (currentClipData.GHClipStages.Count == 0)
            {
                //add main animation to stage base
                currentClipData.AddNextStage();
            }

            {
                GUI.skin.label.alignment = TextAnchor.MiddleLeft;
                isShowConfig = EditorGUILayout.Foldout(isShowConfig, new GUIContent("Show config"));
                #region Show Config
                if (isShowConfig)
                {
                    #region config data to export csv
                    EditorGUILayout.BeginHorizontal();
                    #region column 1
                    EditorGUILayout.BeginVertical();
                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Time Skill: ", "Đơn vị: giây"));
                    currentClipData.timeSkillEnd = EditorGUILayout.FloatField(currentClipData.timeSkillEnd, GUILayout.Width(70));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Time Current: ", "Đơn vị: giây"));
                    timeCurrent = EditorGUILayout.FloatField(timeCurrent, GUILayout.Width(70));
                    timeCurrent = Mathf.Clamp(timeCurrent, 0f, currentClipData.timeSkillEnd);
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Sum_Time: ", "Đơn vị: mili giây"));
                    currentClipData.sum_time = EditorGUILayout.FloatField(currentClipData.sum_time, GUILayout.Width(70));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Break_Time: ", "Đơn vị: mili giây"));
                    currentClipData.break_time = EditorGUILayout.FloatField(currentClipData.break_time, GUILayout.Width(70));
                    EditorGUILayout.EndHorizontal();
                    EditorGUILayout.EndVertical();
                    #endregion

                    #region column 2
                    EditorGUILayout.BeginVertical();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Name Skill: "));
                    currentClipData.nameSkill = EditorGUILayout.TextField(currentClipData.nameSkill, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Id Skill: "));
                    currentClipData.idSkill = EditorGUILayout.IntField(currentClipData.idSkill, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Distance: "));
                    currentClipData.distance = EditorGUILayout.FloatField(currentClipData.distance, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Max_Distance: "));
                    currentClipData.max_distance = EditorGUILayout.FloatField(currentClipData.max_distance, GUILayout.Width(80));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();
                    #endregion

                    #region column 3
                    EditorGUILayout.BeginVertical();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Countdown: ", "Đơn vị: mili giây"));
                    currentClipData.countdown = EditorGUILayout.FloatField(currentClipData.countdown, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Target_Type: "));
                    currentClipData.targetType = (TargetType)EditorGUILayout.EnumPopup(currentClipData.targetType, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();


                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Range_Type: "));
                    currentClipData.rangeType = (RangeType)EditorGUILayout.EnumPopup(currentClipData.rangeType, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();


                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Range: "));
                    currentClipData.range = EditorGUILayout.TextField(currentClipData.range, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();
                    #endregion

                    #region column 4
                    EditorGUILayout.BeginVertical();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Max_Num: "));
                    currentClipData.max_num = EditorGUILayout.IntField(currentClipData.max_num, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Knock_Back: "));
                    currentClipData.knock_back_ID = (KnockBack)EditorGUILayout.EnumPopup(currentClipData.knock_back_ID, GUILayout.Width(150f));
                    EditorGUILayout.EndHorizontal();

                    EditorGUILayout.BeginHorizontal();
                    GUILayout.Label(new GUIContent("Note: ", "Skill có hiệu ứng khác như Thiêu đốt, Hồi máu, Hất tung, Choáng..."));
                    currentClipData.noteSkill = EditorGUILayout.TextField(currentClipData.noteSkill, GUILayout.Width(150f));

                    EditorGUILayout.EndHorizontal();

                    //EditorGUILayout.BeginHorizontal();
                    //GUILayout.Label(new GUIContent("Distance: "));
                    //currentClipData.max_num = EditorGUILayout.IntField( currentClipData.max_num, GUILayout.Width(100f));
                    //EditorGUILayout.EndHorizontal();

                    EditorGUILayout.EndVertical();
                    #endregion

                    EditorGUILayout.EndHorizontal();
                    #endregion
                }
                #endregion
            }
        }

        private void CreateNewPrefab(GameObject obj, string localPath)
        {
#if UNITY_2018_3 || UNITY_2018_4 || UNITY_2018_5 || UNITY_2018_6 || UNITY_2018_7 || UNITY_2018_8 || UNITY_2018_9 || UNITY_2019 || UNITY_2020 || UNITY_2021 || UNITY_2022 || UNITY_2023 || UNITY_2024 || UNITY_2025
		PrefabUtility.SaveAsPrefabAssetAndConnect(obj, localPath, InteractionMode.AutomatedAction);
#else
            UnityEngine.Object prefab = PrefabUtility.CreateEmptyPrefab(localPath);
            PrefabUtility.ReplacePrefab(obj, prefab, ReplacePrefabOptions.ConnectToPrefab);
#endif

        }

        // a method to simplify adding menu items
        void AddMenuItemForColor(GenericMenu menu, string menuPath, SkillElementType skillElementType)
        {
            menu.AddItem(new GUIContent(menuPath), false, OnTypeSelected, skillElementType);
        }

        // the GenericMenu.MenuFunction2 event handler for when a menu item is selected
        void OnTypeSelected(object skillElementType)
        {
            gHCreateSkillData.CreateSkillElementType = (SkillElementType)skillElementType;
            switch (gHCreateSkillData.CreateSkillElementType)
            {
                case SkillElementType.Damage:
                    //addData = new DamageData(gHCreateSkillData.transform);
                    gHCreateSkillData.CreateTypeSkill<DamageData>();
                    break;
                case SkillElementType.Particles:
                    //addData = new ParticleData(gHCreateSkillData.transform);
                    gHCreateSkillData.CreateTypeSkill<ParticleData>();
                    break;
                case SkillElementType.Function:
                    //addData = new FunctionData(gHCreateSkillData.transform);
                    gHCreateSkillData.CreateTypeSkill<FunctionData>();
                    break;
                case SkillElementType.Buff:
                    //addData = new FunctionData(gHCreateSkillData.transform);
                    gHCreateSkillData.CreateTypeSkill<BuffData>();
                    break;
            }
          // gHCreateSkillData.GetAnimaClipDataCurrent().GHSkillStateDatas.Add(addData.MainObj);
        }

        private void drawLine(Rect postion)
        {
            Color oldColor = GUI.color;
            GUI.color = Color.cyan / 2;
            EditorGUI.LabelField(postion, "", GUI.skin.horizontalSlider);
            GUI.color = oldColor;
        }
        #endregion
        #region render Stage
        float WidthOffset = 20;
        float endScrollView;
        float offset2 = 80;

        readonly float WidthCaret = 2;
        readonly float HeightCaret = 360;
        Rect timelineRect;
        private PreviewDataCSV windowPreviewCSV;

        private void GUIStageAnimationClip()
        {

            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();
            {
                EditorGUILayout.BeginHorizontal();
                if (SkillStyleEditor.DrawButtonIcon(SkillStyleEditor.PlusTexture, new Vector2(20, 20), "Add Stage Skill"))
                {
                    if (currentClipData.GHClipStages.Count >= maxStage)
                    {
                        GHSkillEditor.ShowNotification(new GUIContent("Max Stage = " + maxStage));
                        return;
                    }
                    currentClipData.AddNextStage();
                }
                EditorGUILayout.LabelField("Add Stage", EditorStyles.miniBoldLabel);
                EditorGUILayout.EndHorizontal();
                EditorGUILayout.Space();

                float pos = TimeToPos(timeCurrent);
                rectSlider = new Rect(pos - 10, 5, 20, 20);
                windowHeight = GUILayoutUtility.GetLastRect();

                timelineRect = new Rect(WidthOffset, windowHeight.yMax + 5, (widthRect + WidthOffset), heightMin);

                GUI.color = Color.green.WithAlpha(0.2f);
                Rect containerTimeline = new Rect(timelineRect.x, timelineRect.y, timelineRect.width * scale + WidthOffset, timelineRect.height);
                GUI.DrawTexture(containerTimeline, EditorGUIUtility.whiteTexture);
                GUI.color = Color.white;

                GUI.color = Color.black.WithAlpha(0.5f);
                var minHeighTime = 20;
                // show time current
                Rect rectToolBar = new Rect(WidthOffset * 2, timelineRect.y + minHeighTime / 2, widthRect * scale, 3);
                GUI.DrawTexture(rectToolBar, EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
                GUI.color = Color.black;

                var drawRectSlider = rectSlider;
                drawRectSlider.x += (WidthOffset * 2);
                drawRectSlider.y = timelineRect.y;
                GUI.DrawTexture(drawRectSlider, SkillStyleEditor.caretIcon, ScaleMode.StretchToFill, true);

                var rectLabelTimeCurrent = drawRectSlider;
                rectLabelTimeCurrent.y -= 20f;
                rectLabelTimeCurrent.width = 60;
                rectLabelTimeCurrent.height = 30;
                rectLabelTimeCurrent.x -= WidthOffset / 2;
                timeCurr = new TimeSpan(0, 0, (int)timeCurrent);
                GUI.Label(rectLabelTimeCurrent, ConvertTimeSpanToString(timeCurr));

                drawRectSlider.y = timelineRect.y + minHeighTime;

                GUI.DrawTexture(new Rect(TimeToPos(timeCurrent) + WidthOffset * 2 - WidthCaret / 2, drawRectSlider.y + rectSlider.center.y - minHeighTime * 1.5f, WidthCaret, HeightCaret), EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
                //GUI.DrawTexture(new Rect(0, rectSlider.center.y, 2, 600), EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);

                GUI.color = Color.white;
                #region draw timeline

                ratioInterval = modulos[(int)Mathf.Floor(currentClipData.timeSkillEnd / 18)];
                ratioInterval = Mathf.Clamp(ratioInterval, 0, ratioInterval);
                timeSkillInterval = modulosInterval[(int)Mathf.Floor(currentClipData.timeSkillEnd / 18)];
                timeSkillInterval = Mathf.Clamp(timeSkillInterval, 0, timeSkillInterval);
                //Debug.Log("ratioInterval = " + ratioInterval + "    _    timeSkillInterval = " + timeSkillInterval);

                for (var i = timeSkillStart; i <= currentClipData.timeSkillEnd; i += timeSkillInterval)
                {
                    var posX = TimeToPos(i);
                    //Debug.Log("i = " + i);
                    Rect markRect;
                    if (i % ratioInterval == 0)
                    {
                        var timeSpanTimeline = new TimeSpan(0, 0, (int)i);
                        GUI.color = IsProSkin ? Color.blue : Color.black;
                        markRect = Rect.MinMaxRect((posX + WidthOffset * 2 - WidthCaret / 2), drawRectSlider.y + minHeighTime / 2, (posX + WidthOffset * 2 + WidthCaret / 2), drawRectSlider.y + minHeighTime / 2 + 30);
                        var text = ConvertTimeSpanToString(timeSpanTimeline);
                        var size = GUI.skin.GetStyle("label").CalcSize(new GUIContent(text));
                        var stampRect = new Rect(0, 0, size.x, size.y);
                        stampRect.center = new Vector2(posX + WidthOffset * 2, drawRectSlider.y + minHeighTime / 2 - 8);
                        GUI.color = Color.blue;
                        GUI.Box(stampRect, text, (GUIStyle)"label");
                        GUI.color = Color.white;
                    }
                    else
                    {
                        GUI.color = IsProSkin ? Color.blue : Color.black;
                        markRect = Rect.MinMaxRect((posX + WidthOffset * 2), drawRectSlider.y + minHeighTime / 2 + 10, (posX + WidthOffset * 2 + 2), drawRectSlider.y + minHeighTime / 2 + 20);
                        GUI.color = Color.white;
                    }
                    GUI.DrawTexture(markRect, EditorGUIUtility.whiteTexture, ScaleMode.StretchToFill, true);
                    GUI.color = Color.white;

                }
                #endregion
                Rect rectScrollView = timelineRect;
                rectScrollView.yMin += heightMin;
                rectScrollView.yMax += 300;
                rectScrollView.xMax += WidthOffset * 3;

                HanldeEvent(containerTimeline, rectScrollView);

                var countGHClipStages = currentClipData.GHClipStages.Count;
                var countGHSkillStateDatas = currentClipData.GHSkillStateDatas.Count;
                endScrollView = countGHClipStages * 38 + offset2 + countGHSkillStateDatas * 85;
                rectScrollView.width = rectScrollView.width * scale;
                if (rectScrollView.height > endScrollView)
                {
                    scrollViewPos = GUI.BeginScrollView(rectScrollView, scrollViewPos, new Rect(0, 0, 0, rectScrollView.height), false, false);
                }
                else
                {
                    scrollViewPos = GUI.BeginScrollView(rectScrollView, scrollViewPos, new Rect(0, 0, 0, endScrollView), false, false);
                }

                #region draw stage skill
                EditorGUI.BeginChangeCheck();
                var continueHeigh = 0f;
                for (int i = 0; i < currentClipData.GHClipStages.Count;)
                {
                    int countState = currentClipData.GHClipStages.Count;
                    var clipStage = currentClipData.GHClipStages[i];
                    if (clipStage != null)
                    {
                        continueHeigh += 20;
                        Rect rectLabel = new Rect(WidthOffset, continueHeigh, 100, 20);
                        GUI.Label(rectLabel, new GUIContent("Stage " + (i + 1).ToString()), EditorStyles.boldLabel);
                        rectLabel.x += 120f;
                        EditorGUI.LabelField(rectLabel, "First Time: ");
                        rectLabel.x += 70f;
                        clipStage.FirstTime = EditorGUI.FloatField(rectLabel, clipStage.FirstTime);
                        rectLabel.x += 120f;
                        EditorGUI.LabelField(rectLabel, "Last Time: ");
                        rectLabel.x += 70f;
                        clipStage.LastTime = EditorGUI.FloatField(rectLabel, clipStage.LastTime);
                        continueHeigh += 18;
                        Rect rect = new Rect(WidthOffset, continueHeigh, widthRect * scale, 10);
                        EditorGUI.MinMaxSlider(rect, ref clipStage.FirstTime, ref clipStage.LastTime, minLimitSecond, currentClipData.timeSkillEnd);

                        rect.xMin = rect.xMax + 10;
                        rect.xMax = rect.xMin + 15;
                        rect.yMax += 5;
                        if (GUI.Button(rect, SkillStyleEditor.MinusTexutre))
                        {
                            currentClipData.RemoveStageAt(i);
                            //CheckAndResetStageClip();
                            continue;
                        }
                    }
                    i++;
                }
                if (EditorGUI.EndChangeCheck())
                {
                    CheckAndResetStageClip();
                }
                #endregion
                continueHeigh += offset2 / 2;
                Rect rectLabelCurrentClip = new Rect(WidthOffset, continueHeigh, 250, 20);
                GUI.Label(rectLabelCurrentClip, new GUIContent("Length Time Of Current Clip: " + currentClipData.Clip.length), EditorStyles.boldLabel);
                continueHeigh += 15;

                Rect rectMainAnim = new Rect(WidthOffset, continueHeigh, widthRect * scale, 10);

                EditorGUI.MinMaxSlider(rectMainAnim, ref currentClipData.AnimStages.FirstTime, ref currentClipData.AnimStages.LastTime, minLimitSecond, currentClipData.timeSkillEnd);
                currentClipData.AnimStages.LastTime = currentClipData.AnimStages.FirstTime + currentClipData.AnimStages.Length;
                continueHeigh += offset2 / 2;
                {
                    for (int i = 0; i < currentClipData.GHSkillStateDatas.Count;)
                    {
                        var skillData = currentClipData.GHSkillStateDatas[i].GetComponent<GHSkillStateData>();
                        {
                            if (skillData != null)
                            {
                                drawLine(new Rect(WidthOffset, continueHeigh - 15, widthRect * scale, 10));
                                {
                                    Rect rectState = new Rect(WidthOffset, continueHeigh, widthRect * scale, 10);
                                    EditorGUI.BeginChangeCheck();
                                    EditorGUI.MinMaxSlider(rectState, ref skillData.AmountFirst, ref skillData.AmountLast, minLimitSecond, currentClipData.timeSkillEnd);
                                    if (EditorGUI.EndChangeCheck())
                                    {
                                        skillData.CheckTime();
                                    }
                                    rectState.xMin = rectState.xMax + 10;
                                    rectState.xMax = rectState.xMin + 15;
                                    rectState.yMax += 5;
                                    if (GUI.Button(rectState, SkillStyleEditor.MinusTexutre))
                                    {
                                        DeleteGo(currentClipData.GHSkillStateDatas[i]);
                                        currentClipData.GHSkillStateDatas.RemoveAt(i);
                                        continue;
                                    }
                                }
                                {
                                    Rect rectInfo = new Rect(WidthOffset, continueHeigh + 20, 300, 20);
                                    skillData.Render(i, rectInfo);
                                    continueHeigh += offset2;
                                }
                            }
                        }
                        i++;
                    }
                }
                GUI.EndScrollView();                                                
                                       
            }
            EditorGUILayout.Space();
           
        }

        private void CheckAndResetStageClip()
        {
            var currentClipData = gHCreateSkillData.GetAnimaClipDataCurrent();
            for (int i = 0; i < currentClipData.GHClipStages.Count; i++)
            {
                int countState = currentClipData.GHClipStages.Count;
                var clipStage = currentClipData.GHClipStages[i];
                if (i < countState - 1)
                {
                    if (clipStage.LastTime > currentClipData.GHClipStages[i + 1].LastTime)
                    {
                        clipStage.LastTime = currentClipData.GHClipStages[i + 1].LastTime;
                    }

                }
                if (clipStage.FirstTime > clipStage.LastTime)
                {
                    clipStage.OldFirstTime = clipStage.LastTime;
                    clipStage.FirstTime = clipStage.LastTime;
                    clipStage.OldLastTime = clipStage.LastTime;
                }
                if (clipStage.FirstTime != clipStage.OldFirstTime)
                {
                    if (i == 0)
                    {
                        clipStage.OldFirstTime = clipStage.FirstTime;
                    }
                    if (i >= 1 && currentClipData.GHClipStages[i - 1] != null)
                    {
                        if (clipStage.FirstTime <= currentClipData.GHClipStages[i - 1].FirstTime)
                        {
                            clipStage.FirstTime = currentClipData.GHClipStages[i - 1].FirstTime;
                        }
                        currentClipData.GHClipStages[i - 1].LastTime = clipStage.FirstTime;
                        currentClipData.GHClipStages[i - 1].OldLastTime = clipStage.FirstTime;
                        clipStage.OldFirstTime = clipStage.FirstTime;
                    }
                }
                if (clipStage.LastTime != clipStage.OldLastTime)
                {
                    if (i == countState - 1)
                    {
                        clipStage.OldLastTime = clipStage.LastTime;
                        currentClipData.UpdateStage();
                        continue;
                    }
                    if (currentClipData.GHClipStages[i + 1] != null)
                    {
                        if (clipStage.LastTime >= currentClipData.GHClipStages[i + 1].LastTime)
                        {
                            clipStage.LastTime = currentClipData.GHClipStages[i + 1].LastTime;
                        }
                        currentClipData.GHClipStages[i + 1].FirstTime = clipStage.LastTime;
                        currentClipData.GHClipStages[i + 1].OldFirstTime = clipStage.LastTime;
                        clipStage.OldLastTime = clipStage.LastTime;
                    }
                }
                if (Mathf.Abs(timeCurrent - clipStage.FirstTime) <= deltaStage)
                {
                    clipStage.FirstTime = timeCurrent;
                    clipStage.OldFirstTime = clipStage.FirstTime;
                }
                if (Mathf.Abs(timeCurrent - clipStage.LastTime) <= deltaStage)
                {
                    clipStage.LastTime = timeCurrent;
                    clipStage.OldLastTime = clipStage.LastTime;
                }
                currentClipData.UpdateStage();

            }
        }

        private string ConvertTimeSpanToString(TimeSpan span)
        {
            string formatted = string.Format("{0}{1}{2}{3}",
       span.Duration().Days > 0 ? string.Format("{0:00}:", span.Days) : string.Empty,
       span.Duration().Hours > 0 ? string.Format("{0:00}:", span.Hours) : string.Empty,
       span.Duration().Minutes > 0 ? string.Format("{0:00}:", span.Minutes) : "00:",
       span.Duration().Seconds > 0 ? string.Format("{0:00}", span.Seconds) : "00");

            if (string.IsNullOrEmpty(formatted)) formatted = "00:00";

            return formatted;
        }
        #region handler event
        private void HanldeEvent(Rect rectCastEvent, Rect rectScrollView)
        {
            var e = Event.current;
            if (e.type == EventType.ScrollWheel)
            {
                isScrollTimeline = true;
                e.Use();
            }
            else
            {
                isScrollTimeline = false;
            }

            mousePosition = e.mousePosition;
            mousePosition.x -= WidthOffset;
            if (e.type == EventType.MouseDown && rectCastEvent.Contains(mousePosition)
                && isDragHandler == false)
            {
                isDragHandler = true;
                UpdateHandler();
                foreach (var item in gHCreateSkillData.GetAnimaClipDataCurrent().GHSkillStateDatas)
                {
                    var ghSkillState = item.GetComponent<GHSkillStateData>();
                    if (timeCurrent < ghSkillState.AmountFirst)
                    {
                        ghSkillState.isPlayed = false;
                    }
                }
              //  gHCreateSkillData.ResetIsPlayedOfClipDataCurrent();
                e.Use();
            }
            if (e.type == EventType.MouseDrag && isDragHandler)
            {
                if (e.button == 0)
                {
                    UpdateHandler();
                    var delta = e.delta.x;
                    if (delta >= 0)
                    {
                        if (mousePosition.x > rectScrollView.width + scrollViewPos.x - WidthOffset * 2)
                        {
                            scrollViewPos.x += (10f + 10 * scale);
                        }
                    }
                    else
                    {
                        if (mousePosition.x < (scrollViewPos.x + WidthOffset * 2))
                        {
                            scrollViewPos.x -= (10f + 10 * scale);
                        }
                    }
                    
                }
                e.Use();
            }
            if (e.type == EventType.MouseUp)
            {
                isDragHandler = false;
            }

            if (isScrollTimeline)
            {
                if (e.delta.y < 0)
                {
                    scale -= 0.01f;
                }
                else
                {
                    scale += 0.01f;
                }
                float pos = TimeToPos(timeCurrent);
                rectSlider = new Rect(pos - 10, 5, 20, 20);
            }
        }

        private void UpdateHandler()
        {
            float mousePos = Mathf.Clamp(mousePosition.x, WidthOffset, widthRect * scale + WidthOffset);
            rectSlider = new Rect(mousePos - WidthOffset - 10, 5, 20, 20);
            timeCurrent = PosToTime(rectSlider.x + 10);
        }
        float TimeToPos(float time)
        {
            float ratio = widthRect * scale / gHCreateSkillData.GetAnimaClipDataCurrent().timeSkillEnd;
            float pos = time * ratio;
            return pos;
        }

        float PosToTime(float pos)
        {
            float ratio = widthRect * scale / gHCreateSkillData.GetAnimaClipDataCurrent().timeSkillEnd;
            float time = pos / ratio;
            return time;
        }
        #endregion
        #endregion
        #region common
        void ToggleAnimationMode()
        {
            if (AnimationMode.InAnimationMode())
                AnimationMode.StartAnimationMode();
            else
                AnimationMode.StartAnimationMode();
        }

        #endregion

        public void OnDisable()
        {
            EditorApplication.update -= UpdateParticle;
            EditorApplication.update -= UpdateDamage;
        }
    }

}
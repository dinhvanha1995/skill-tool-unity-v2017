﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using UnityEditor.IMGUI.Controls;

namespace GHTools.SkillCreate.Editor
{
	public class GHSkillEditor : EditorWindow
	{
        //params
        private static MainSkillEditor mainEditor;
        private static GHSkillEditor window;
        [MenuItem("Custom Editor/GH Skill Editor")]
		public static void SkillWindow()
		{
			window = GetWindow<GHSkillEditor>(typeof(GHSkillEditor));
			window.name = "GH Skill Editor";
			window.minSize = new Vector2(800, 800);
			window.maxSize = new Vector2(1600, 1600);
			window.Show();
            SkillStyleEditor.Load();
        }
    
        void OnGUI()
		{
            if (EditorApplication.isCompiling)
            {
                ShowNotification(new GUIContent("Please\n ...Is Compiling..."));
                return;
            }
            if (mainEditor == null)
			{
				mainEditor = new MainSkillEditor();
            }
            if(window == null)
            {
                window = GetWindow<GHSkillEditor>(typeof(GHSkillEditor));
            }
            if (mainEditor.GHSkillEditor == null)
            {
                mainEditor.GHSkillEditor = window;
            }
            //Draw Setting Scene
            mainEditor.GUISkillEditor();

        }
		void Update()
		{
			if (mainEditor == null)
			{
				mainEditor = new MainSkillEditor();
			}
			mainEditor.Update();
			if (mainEditor.isPlay)
			{
				Repaint();
			}
		}

        private void OnDisable()
        {
           // mainEditor.OnDisable();
        }
    }
}
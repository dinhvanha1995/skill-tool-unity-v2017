﻿using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
using System.Linq;
using System;
using System.IO;

namespace GHTools.SkillCreate
{
    public class GHCreateSkillData : MonoBehaviour
    {
        public static string formatDataCSV = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}" + System.Environment.NewLine,
                                    "stage", "id", "name", "target_type", "cd", "distance", "max_distance", "sum_time", "break_time",
                                    "action_id", "effect_id|bind", "stage_time", "attack_time", "max_num", "target_type", "range_type", "range", "dmg", "knock_back_ID"
                                    , "start_stage", "end_stage", "des_stage");

        //tools
        [HideInInspector] public static string PathExportCSV;
        [HideInInspector] public static string PathExportPrefab;
        [HideInInspector] public static string OUT_PATH_CSV = "OutputPathCSV";
        [HideInInspector] public static string OUT_PATH_PREFAB = "OutputPathPrefab";

        private void OnEnable()
        {
#if UNITY_EDITOR
            PathExportCSV = EditorPrefs.GetString(OUT_PATH_CSV, Application.dataPath);
            PathExportPrefab = EditorPrefs.GetString(OUT_PATH_PREFAB, Application.dataPath);
#endif
        }
        // GUI Setting On Scene
        public ModelData Environment;
        public ModelData CharacterData;
        public ModelData TargetData;
        //logic
        public SkillTargetMode SkillTargetMode;
        public float DistanceCharacterVsTarget = 5f;

        public SkillElementType CreateSkillElementType;

        //camera
        public CameraViewMode CameraViewMode;

        public bool isLoop = false;

        // GUI Show Animation
       // public float MainAnimationSlider;

       
        // Animation Clip Tab
        public List<AnimationClip> AnimationClips;


        #region Animation clip data
        public int IndexClipSelected = 0;
        public List<GHAnimClipData> GHAnimClipDataList;

        public GHAnimClipData GetAnimaClipDataCurrent()
        {
            if (GHAnimClipDataList != null && GHAnimClipDataList.Count > IndexClipSelected)
            {
                return GHAnimClipDataList[IndexClipSelected];
            }
            return null;
        }

        public void ResetIsPlayedOfClipDataCurrent()
        {
            var clipDataCurrent = GetAnimaClipDataCurrent();
            foreach (var dataObj in clipDataCurrent.GHSkillStateDatas)
            {
                var item = dataObj.GetComponent<GHSkillStateData>();
                item.isPlayed = false;
            }
        }


        public void RefreshScene()
        {
            CharacterData.CheckOrCreateModel(UtilLanguage.HERO);
            CharacterData.CheckComponent();
            CharacterData.SetPositionModel(Vector3.zero);
            CharacterData.RotateModel(new Vector3(0,180,0));
           
            //check main target
            if (SkillTargetMode == SkillTargetMode.EnemyInteraction)
            {
                TargetData.CheckOrCreateModel(UtilLanguage.ENEMY);
                TargetData.SetPositionModel(-Vector3.forward * DistanceCharacterVsTarget);
                TargetData.RotateModel(new Vector3(0, 0, 0));
            }
        }

        #endregion
        #region Utils
        public int ConvertLengthAnim(float time)
        {
            return (int)(time * 100);
        }
        #endregion

        public void CreateTypeSkill<T>() where T : GHSkillStateData
        {
            GameObject MainObj = new GameObject();
            MainObj.transform.SetParent(this.transform);
            T componentData = MainObj.AddComponent<T>();
            componentData.Init(this);
            GetAnimaClipDataCurrent().GHSkillStateDatas.Add(MainObj);
        }
        public void ExportCSVFile(string nameFile)
        {
            StartCoroutine(CrearArchivoCSV(nameFile));
        }

        private IEnumerator CrearArchivoCSV(string nameFile)
        {
            string file = PathExportCSV + "/" + nameFile + ".csv";
            if (File.Exists(file))
            {
                File.SetAttributes(file, FileAttributes.Normal);
                File.Delete(file);              //Crear el archivo
            }

            string datosCSV = formatDataCSV;

            foreach (var clipData in GHAnimClipDataList)
            {
                if (clipData != null)
                {
                    string stateData = string.Empty;
                    string particleData = string.Empty;
                    string damageData = string.Empty;
                    string funcData = string.Empty;
                    string stageId = string.Empty;
                    int idSkill = clipData.idSkill;
                    string nameSkill = "\'" + clipData.nameSkill + "\'";
                    int targetType = (int)clipData.targetType;
                    float cd = clipData.countdown;
                    float distance = clipData.distance;
                    float max_distance = clipData.max_distance;
                    float sum_time = clipData.sum_time;
                    float break_time = clipData.break_time;
                    string actionId = "\'" + clipData.Clip.name + "\'";
                    int max_num = clipData.max_num;
                    int range_type = (int)clipData.rangeType;
                    string range = (clipData.range == null || string.IsNullOrEmpty(clipData.range)) ? "{}" : clipData.range;
                    int knock_back_ID = (int)clipData.knock_back_ID;
                    // datosCSV += System.Environment.NewLine;
                    for (int i = 0; i < clipData.GHClipStages.Count; i++)
                    {
                        var stage = clipData.GHClipStages[i];
                        stageId = "stage " + (i + 1).ToString();
                        var listComponent = new List<GHSkillStateData>();
                        int countDamage = 0;
                        var stage_time = stage.LastTime - stage.FirstTime;
                        float attack_time = Mathf.Infinity;
                        float start_stage = stage.FirstTime;
                        float last_stage = stage.LastTime;
                        string des_stage = string.Empty;
                        string damage_id = string.Empty;
                        string effect_id = string.Empty;

                        foreach (var objChild in clipData.GHSkillStateDatas)
                        {
                            if (objChild.gameObject.name.Contains("DamageData_"))
                            {
                                DamageData damageCompo = objChild.GetComponent<DamageData>();
                                if (damageCompo != null && listComponent.Contains(damageCompo) == false)
                                {
                                    if (damageCompo.AmountFirst >= stage.FirstTime && damageCompo.AmountFirst < stage.LastTime)
                                    {
                                        attack_time = Mathf.Min(attack_time, damageCompo.AmountFirst - stage.FirstTime);
                                        damage_id += "\'" + damageCompo.damageFake.ToString() + "_" + ((damageCompo.AmountFirst - stage.FirstTime)* 1000).ToString() + "\'" + ",";
                                        countDamage++;
                                        listComponent.Add(damageCompo);
                                    }
                                    //damageData += (damageCompo.AmountFirst * 1000) + "_";

                                }
                            }
                            else if (objChild.gameObject.name.Contains("ParticleData_"))
                            {
                                ParticleData particleCompo = objChild.GetComponent<ParticleData>();
                                if (particleCompo != null && listComponent.Contains(particleCompo) == false)
                                {
                                    if (particleCompo.AmountFirst >= stage.FirstTime && particleCompo.AmountFirst <= stage.LastTime)
                                    {
                                        effect_id += particleCompo.effectObject.name + "|" + particleCompo.bind.ToString() + ",";
                                        listComponent.Add(particleCompo);
                                    }
                                   // particleData += (particleCompo.AmountFirst * 1000) + "_";
                                }
                            }
                            else if (objChild.gameObject.name.Contains("FunctionData_"))
                            {
                                FunctionData funcCompo = objChild.GetComponent<FunctionData>();
                                if (funcCompo != null && listComponent.Contains(funcCompo) == false)
                                {
                                    if (funcCompo.AmountFirst >= stage.FirstTime && funcCompo.AmountFirst <= stage.LastTime)
                                    {
                                        if (des_stage == null || string.IsNullOrEmpty(des_stage))
                                        {
                                            des_stage = funcCompo.des.Substring(0, 1).ToUpper() + funcCompo.des.Substring(1);
                                        }
                                        else
                                        {
                                            des_stage += " Sau đó, " + funcCompo.des;
                                        }
                                        listComponent.Add(funcCompo);
                                    }
                                    // particleData += (particleCompo.AmountFirst * 1000) + "_";
                                }
                            }
                        }
                        if ((damage_id != null || string.IsNullOrEmpty(damage_id) == false) && damage_id.Contains("_"))
                        {
                            damage_id = "\"{" + damage_id.Substring(0, damage_id.Length - 1) + "\"}";
                        }
                        if ((range != null || string.IsNullOrEmpty(range) == false) && range.Contains(","))
                        {
                            range = "\"" + range + "\"";
                        }
                        if ((effect_id != null || string.IsNullOrEmpty(effect_id) == false) && effect_id.Contains("|"))
                        {
                            effect_id = "\"\'" + effect_id.Substring(0, effect_id.Length - 1) + "\"\'";
                        }
                        if ((des_stage != null || string.IsNullOrEmpty(des_stage) == false))
                        {
                            des_stage = "\"\'" + des_stage + "\"\'";
                        }
                        if (attack_time == Mathf.Infinity || attack_time > stage_time)
                        {
                            attack_time = 0;
                        }
                        if (i == 0)
                        {
                            datosCSV += string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}" + System.Environment.NewLine,
                                                               stageId, idSkill, nameSkill, targetType, cd, distance, max_distance, sum_time, break_time,
                                                               actionId, effect_id, stage_time * 1000, attack_time * 1000, max_num, targetType, range_type, range, damage_id, knock_back_ID
                                                               , start_stage * 1000, last_stage * 1000, des_stage);
                        }
                        else
                        {
                            datosCSV += string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},{17},{18},{19},{20},{21}" + System.Environment.NewLine,
                                   stageId, "", "", "", "", "", "", "", "", "", effect_id, stage_time * 1000, attack_time * 1000, "", "", "", "", damage_id, ""
                                   , start_stage * 1000, last_stage * 1000, des_stage);
                        }
                      
                    }
                    datosCSV += System.Environment.NewLine;
                }

            }
            using (StreamWriter filestream = new StreamWriter(File.Open(file, FileMode.OpenOrCreate), System.Text.Encoding.UTF8))
            {
                filestream.WriteLine(datosCSV);
                filestream.Close();
            }


            FileInfo fInfo = new FileInfo(file);
			fInfo.IsReadOnly = true;

            yield return new WaitForSeconds(0.5f);//Esperamos para estar seguros que escriba el archivo
           // Application.OpenURL(file);
        }
    }
    [System.Serializable]
    public class GHClipStage
    {
        public int StageId;
        public float FirstTime;
        public float OldFirstTime;
        public float LastTime;
        public float OldLastTime;
        public float Length;
        public GHClipStage(int _id, float _firstTiem, float _lastTime)
        {
            this.StageId = _id;
            this.FirstTime = _firstTiem;
            this.LastTime = _lastTime;
            this.OldFirstTime = _firstTiem;
            this.OldLastTime = _lastTime;
            this.CalculateLength();
        }
        public void Update()
        {
            this.CalculateLength();
        }
        public void CalculateLength()
        {
            this.Length = LastTime - FirstTime;
        }
    }

    [System.Serializable]
    public class GHAnimClipData
    {
        #region Stage
        //stage data
        public string Name;
        public AnimationClip Clip;
        public float BaseLengthClip;
        public float TotalLengthStage = 0;
        public float timeSkillEnd;
        public TargetType targetType;
        public RangeType rangeType;
        public float countdown;
        public float distance;
        public float max_distance;
        public KnockBack knock_back_ID;
        public float sum_time;
        public float break_time;
        public int max_num;
        public string range;
        public int idSkill;
        public string nameSkill;
        public string noteSkill;

        public List<GameObject> GHSkillStateDatas;
        public List<GHClipStage> GHClipStages;
        public GHClipStage AnimStages;
        
        public GHAnimClipData(AnimationClip _clip)
        {
            this.Clip = _clip;
            this.Name = _clip.name;
            timeSkillEnd = 10f;
            BaseLengthClip = Clip.length;
            GHSkillStateDatas = new List<GameObject>();
            GHClipStages = new List<GHClipStage>();
            AnimStages = new GHClipStage(0, 0, BaseLengthClip);
            targetType = TargetType.Three;
            rangeType = RangeType.Hình_Chữ_Nhật_Phía_Trước;
            countdown = 0;
            sum_time = (float)Math.Round(Clip.length, 1) * 1000;
            break_time = sum_time - 300f;
            idSkill = 101010;
            nameSkill = "Thanh Vân Kiếm";

        }

        //public const int WIDTH_STAGE = 700;
        //public const float MIN_RATIO_STAGE = 0.01f;
        //public const float MAX_RATIO_STAGE = 0.99f;

        public void UpdateStage()
        {
            TotalLengthStage = 0;
            //GHClipStages.ForEach(r => TotalLengthStage += r.Length);
            if (GHClipStages.Count == 0)
            {
                TotalLengthStage = 0;
                return;
            }
            foreach (var item in GHClipStages)
            {
                item.Update();
                TotalLengthStage = Mathf.Max(TotalLengthStage, item.LastTime);
            }
            // Debug.Log("TotalLengthStage = " + TotalLengthStage);
        }
        public void AddBaseStage()
        {
            //add main animation to stage base
            //var currentClipData = GetAnimaClipDataCurrent();
            //BaseLengthClip = ConvertLengthAnim(currentClipData.Clip.length);
            
            //UpdateStage();
        }

        public int GetMaxId()
        {
            int maxId = 0;
            foreach (GHClipStage item in GHClipStages)
            {
                maxId = Mathf.Max(maxId, item.StageId);
            }
            return maxId + 1;
        }

        public void AddNextStage()
        {
            // add next stage
            int id = GetMaxId();
            GHClipStages.Add(new GHClipStage(id, TotalLengthStage, TotalLengthStage + 0.5f));
            //var currentMaxLength = TotalLengthStage + BaseLengthClip;
            UpdateStage();
        }

        public void RemoveStageAt(int index)
        {
            GHClipStages.RemoveAt(index);
            UpdateStage();
        }

        #endregion
    }
   
    

    [System.Serializable]
    public class ModelData
    {
        public GameObject Prefab;
        public Transform Model;
        private string namePrefab;
        public bool IsDefaultRotate;
        public Vector3 CustomRotate;
        public ModelData()
        {
            IsDefaultRotate = true;
        }
        public void CheckOrCreateModel(string name)
        {
            if (Prefab != null)
            {
                if (Model == null)
                {
                    namePrefab = Prefab.name;
                    CreateModel(name);
                    return;
                }
                if (namePrefab != Prefab.name)
                {
                    Reset();
                    CreateModel(name);
                    namePrefab = Prefab.name;
                }
            }
            else
            {
                Debug.LogError(string.Format("{0} is missing", "Prefab"));
                return;
            }
        }

        public void CreateModel(string name)
        {
            Model = new GameObject(name).transform;
            Transform modelChild = GameObject.Instantiate(Prefab).transform;
            modelChild.gameObject.name = modelChild.gameObject.name.Replace(UtilLanguage.OBJGAME_CLONE, "");
            modelChild.parent = Model;
            Model.position = Vector3.zero;
        }

        public void Reset()
        {
            if (Model != null)
            {
                GameObject.DestroyImmediate(Model.gameObject);
                Model = null;
            }
        }

        public void SetPositionModel(Vector3 position)
        {
            Model.position = position;
        }
        public void RotateModel(Vector3 defaultRotate)
        {
            if (IsDefaultRotate)
            {
                Model.eulerAngles = defaultRotate;
            }
            else
            {
                Model.eulerAngles = CustomRotate;
            }
        }

        public void CheckComponent()
        {
            if (Model != null)
            {
                foreach (var item in Model.GetComponents<Component>())
                {
                    if (!(item is Transform))
                    {
                       GameObject.DestroyImmediate(item);
                    }
                }
            }
        }
    }

 
}
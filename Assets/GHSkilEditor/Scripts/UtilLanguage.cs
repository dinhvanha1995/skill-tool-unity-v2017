﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class UtilLanguage
{
    public static string HERO = "Hero";
    public static string ENEMY = "Enemy";
    public static string ENVIRONMENT = "Environment";
    public static string STAGEID = "Số thứ tự stage của skill hiện tại";
    public static string SKILLID = "ID của skill hiện tại";
    public static string NAMESKILL = "Tên của skill";
    public static string CD = "Countdown";
    public static string EFFECTID = "Tên của Effect | Có gắn với transform của Player không?";
    public static string MAXNUM = "Số target tối đa";
    public static string KNOCK_BACK = "Mỗi lần gây dame sẽ run camera";
    public static string DES_STAGE = "Mô tả sơ về cách stage này thi triển. Chuỗi trống tức là stage này bình thường.";
    public static string NAME_GO = "GH_Skill_Data";
    public static string OBJGAME_CLONE = "(Clone)";
}

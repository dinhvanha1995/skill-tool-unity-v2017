﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace GHTools.SkillCreate
{
    public class AbsScript : MonoBehaviour
    {

        [HideInInspector] public TypePosition type;
        [HideInInspector]
        public float firstTime;
        [HideInInspector]
        public float lastTime;
        [HideInInspector] public float offsetX;
        [HideInInspector] public float offsetY;
        [HideInInspector] public float offsetZ;
        [HideInInspector] public bool isAnim = false;
        [HideInInspector] public float time = 0;
        [HideInInspector] public float distance;
        [HideInInspector] public float speed;
        [HideInInspector] public Vector3 targetPositionTemp;
        [HideInInspector]
        public AxisType typeAxis;
        [HideInInspector]
        public Space typeSpace;
        [HideInInspector]
        public bool lookAt;
        public bool ignoreLastTime;


        public GameObject control;
        [HideInInspector] public Transform targetObj;
       
        public void AssignVariable()
        {
            FunctionData func = control.GetComponent<FunctionData>();
            if (func != null)
            {
                firstTime = func.AmountFirst;
                lastTime = func.AmountLast;
                offsetX = func.offsetX;
                offsetY = func.offsetY;
                offsetZ = func.offsetZ;
                type = func.typePos;
                targetObj = func.targetObj;
                distance = func.distance;
                speed = func.speed;
                typeSpace = func.typeSpace;
                typeAxis = func.typeAxis;
                targetPositionTemp = func.targetPositionTemp;
                lookAt = func.lookAt;
                ignoreLastTime = func.ignoreLastTime;
            }
            CheckSpeed();
            time = 0;
        }
        public Vector3 GetPointByVectorAndDistance(Vector3 startPoint, Vector3 direction, float distance)
        {
            float distanceVector = distance / direction.magnitude;
            return direction * distanceVector + startPoint;
        }

        public void LookAt(GameObject role, GameObject target)
        {

            if (lookAt)
            {
                if (target != null)
                {
                    if (target.name == UtilLanguage.ENEMY)
                    {
                        Vector3 dir = Vector3.Normalize(target.transform.position - role.transform.position);
                        if (dir != Vector3.zero)
                        {
                            Quaternion lookRotation = Quaternion.LookRotation(dir);
                            role.transform.rotation = lookRotation;
                        }
                    }
                    else
                    {
                        GameObject enemy = GameObject.Find(UtilLanguage.ENEMY);
                        if (enemy != null)
                        {
                            Vector3 dir = Vector3.Normalize(enemy.transform.position - role.transform.position);
                            if (dir != Vector3.zero)
                            {
                                Quaternion lookRotation = Quaternion.LookRotation(dir);
                                role.transform.rotation = lookRotation;
                            }
                        }
                    }
                }
            }
        }

        public void CheckSpeed()
        {
            if (speed < 0)
            {
                speed = Mathf.Abs(speed);
            }
        }
    }
}

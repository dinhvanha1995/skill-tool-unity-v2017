﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ExampleScript : MonoBehaviour {

    public float xAngle, yAngle, zAngle;
    public Space space;
    private GameObject cube1, cube2;
    public float speed;
    void Awake()
    {
        cube1 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube1.transform.position = new Vector3(0.75f, 0.0f, 0.0f);
        cube1.transform.Rotate(90.0f, 0.0f, 0.0f, Space.Self);
        cube1.GetComponent<Renderer>().material.color = Color.red;
        cube1.name = "Self";

        cube2 = GameObject.CreatePrimitive(PrimitiveType.Cube);
        cube2.transform.position = new Vector3(-0.75f, 0.0f, 0.0f);
        cube2.transform.Rotate(90.0f, 0.0f, 0.0f, Space.World);
        cube2.GetComponent<Renderer>().material.color = Color.green;
        cube2.name = "World";
        yAngle = 1;
        space = Space.World;
        speed = 10f;
    }

    void Update()
    {
      //  cube1.transform.Rotate(xAngle, yAngle, zAngle, Space.Self);
        cube1.transform.Rotate(new Vector3( xAngle, yAngle, zAngle), speed * Time.deltaTime, space);
        cube2.transform.Rotate(xAngle, yAngle, zAngle, Space.World);
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GHTools.SkillCreate
{
    [ExecuteInEditMode]
    public class RotateScript : AbsScript
    {
        private Vector3 axis;
        // Use this for initialization
        void Start()
        {
            AssignVariable();
            switch (typeAxis)
            {
                case AxisType.X:
                    axis = Vector3.right;
                    break;
                case AxisType.Y:
                    axis = Vector3.up;
                    break;
                case AxisType.Z:
                    axis = Vector3.forward;
                    break;
                default:
                    axis = Vector3.up;
                    break;
            }
        }

        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;

            if (axis != null)
            {
                transform.Rotate(axis, speed, typeSpace);
            }
            if (time >= (lastTime - firstTime))
            {
                DestroyImmediate(this);
            }
        }
    }
}

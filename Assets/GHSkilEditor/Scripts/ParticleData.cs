﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GHTools.SkillCreate
{
    public class ParticleData : GHSkillStateData
    {
        public bool bind;
        public override void ExcuteState()
        {
            if (effectObject == null) return;
            if (ParticlePlayer.amountEff < ParticlePlayer.maxEff)
            {
                GameObject particleObj = Instantiate<GameObject>(effectObject);
                ParticlePlayer particlePlayer = particleObj.AddComponent<ParticlePlayer>();
                particlePlayer.bind = bind;
                SettingPosEffect(particleObj, SkillTargetType);
                particleObj.transform.rotation = parentControl.CharacterData.Model.rotation;
                ParticlePlayer.amountEff++;
            }
        }
        public override void Render(int index, Rect position)
        {
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            // xuống dòng
            Rect tempPosition = position;
            EditorGUI.LabelField(tempPosition, index + ".Target Type");
            tempPosition = SpaceParameter(position, 2);
            SkillTargetType = (SkillTargetType)EditorGUI.EnumPopup(tempPosition, SkillTargetType);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "First Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountFirst = EditorGUI.FloatField(tempPosition, AmountFirst);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "Last Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountLast = EditorGUI.FloatField(tempPosition, AmountLast);
            // xuống dòng
            position.y += 25;
            Rect tempPos1 = position;
            EditorGUI.LabelField(tempPos1, index + ".Particles Object");
            tempPos1 = SpaceParameter(position, 2.5f);
            effectObject = (GameObject)EditorGUI.ObjectField(tempPos1, effectObject, typeof(GameObject), false);
            tempPos1 = SpaceParameter(tempPos1, 2.5f);
            EditorGUI.LabelField(tempPos1, new GUIContent("Bind: ", "Move effect with player"));

            tempPos1 = SpaceParameter(tempPos1, 1f);
            bind = EditorGUI.Toggle(tempPos1, bind);
            if (EditorGUI.EndChangeCheck())
            {
                CheckCondition();
                CheckTime();
            }
#endif
        }

        private void CheckCondition()
        {
            if (SkillTargetType == SkillTargetType.Enemy && bind == true)
            {
                Debug.LogError("Bind và target Enemy không thể đi chung. Điều này sẽ tự động chuyển target thành My Self.");
                SkillTargetType = SkillTargetType.MySelf;
            }
        }

        public override void Init(GHCreateSkillData gHCreateSkillData)
        {
            base.Init(gHCreateSkillData);
            name = "ParticleData_" + Random.Range(0, 100);
            SkillElementType = SkillElementType.Particles;
            effectObject = Resources.Load<GameObject>("Effect/SkillEffect/god_001_attack01");
            bind = false;
        }
    }
}

﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GHTools.SkillCreate
{
    public class DamageData : GHSkillStateData
    {
        //damage
        public int damageFake;
        public Color colorDamage;
        public override void ExcuteState()
        {
            if (effectObject == null) return;
            GameObject damageObj = Instantiate<GameObject>(effectObject);
            DamageText damageText = damageObj.AddComponent<DamageText>();
            damageText.control = gameObject;
            SettingPosEffect(damageObj, SkillTargetType);
#if UNITY_EDITOR
            //damageObj.transform.forward = Camera.main.transform.forward;
            UnityEditor.SceneView sceneView = (UnityEditor.SceneView)UnityEditor.SceneView.sceneViews[0];
            damageObj.transform.forward = sceneView.camera.transform.forward;
            if (parentControl.GetAnimaClipDataCurrent().knock_back_ID > 0)
            {
                if (UnityEditor.EditorApplication.isPlaying)
                {
                    //Camera.main.gameObject.AddComponent<StressReceiver>().InduceStress(0.5f);
                }
                else
                {
                    sceneView.camera.gameObject.AddComponent<StressReceiver>().InduceStress(0.5f);
                }
            }
#endif
        }

        public override void Render(int index, Rect position)
        {
#if UNITY_EDITOR

            EditorGUI.BeginChangeCheck();
            // xuống dòng
            Rect tempPosition = position;
            EditorGUI.LabelField(tempPosition, index + ".Target Type");
            tempPosition = SpaceParameter(position, 2);
            SkillTargetType = (SkillTargetType)EditorGUI.EnumPopup(tempPosition, SkillTargetType);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "First Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountFirst = EditorGUI.FloatField(tempPosition, AmountFirst);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "Last Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountLast = EditorGUI.FloatField(tempPosition, AmountLast);

            // xuống dòng
            position.y += 25;
            Rect tempPos1 = position;
            EditorGUI.LabelField(tempPos1, index + ".Damage Object");
            tempPos1 = SpaceParameter(tempPos1, 2.5f, 130);
            effectObject = (GameObject)EditorGUI.ObjectField(tempPos1, effectObject, typeof(GameObject), false);
            tempPos1 = SpaceParameter(tempPos1, 3, 120);
            EditorGUI.LabelField(tempPos1, new GUIContent("Color:", "Color of text damage"));
            tempPos1 = SpaceParameter(tempPos1, 1.5f);
            colorDamage = EditorGUI.ColorField(tempPos1, colorDamage);
            tempPos1 = SpaceParameter(tempPos1, 3);
            EditorGUI.LabelField(tempPos1, "Text Damage:");
            tempPos1 = SpaceParameter(tempPos1, 2);
            damageFake = EditorGUI.IntField(tempPos1, damageFake);
            if (EditorGUI.EndChangeCheck())
            {
                CheckTime();
            }
#endif
        }
        public override void Init(GHCreateSkillData gHCreateSkillData)
        {
            base.Init(gHCreateSkillData);
            name = "DamageData_" + UnityEngine.Random.Range(0, 100);
            SkillElementType = SkillElementType.Damage;
            SkillTargetType = SkillTargetType.Enemy;
            damageFake = 200;
            effectObject = (GameObject)Resources.Load("Damage");
            colorDamage = Color.red;
        }

    }

}

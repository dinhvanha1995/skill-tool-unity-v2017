﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GHTools.SkillCreate
{
	public enum SkillTargetMode
	{
		OnlyCharacter,
		EnemyInteraction,
	}
	public enum CameraViewMode
	{
		InFront,
		Behind,
		Right, 
		Left
	}

	public enum SkillElementType
	{
		Damage,
		Particles,
		Function,
        Buff,
	}

	public enum SkillTargetType
	{
		MySelf,
		Enemy
	}

    public enum BuffType
    {
        Hất_Tung,
        Choáng,
        Kéo,
        Đẩy,
    }

    public enum TypePosition
    {
        Front, Behind, Left, Right
    }

    public enum ScriptSkill
    {
        Teleport,
        Moving,
        Rotate,
        Vanish,
    }
    public enum TargetType
    {
        One = 1, Two = 2, Three = 3
    }
    public enum AxisType
    {
        X = 1, Y = 2, Z = 3
    }
    public enum KnockBack
    {
        None = 0,
        Run_Màn_Hình = 401,
        Giật_Lui_Và_Run_Màn_Hình = 501,
    }
    public enum RangeType
    {
        Đơn_Mục_Tiêu_Không_Đường_Đạn = 1,
        Đơn_Mục_Tiêu_Có_Đường_Đạn = 2,
        Hình_Chữ_Nhật_Phía_Trước = 3,
        Hình_Quạt_Phía_Trước = 4,
        Vòng_Tròn_Quanh_Bản_Thân = 5,
        Vòng_Tròn_Quanh_Đối_Thủ_Không_Đường_Đạn = 6,
        Vòng_Tròn_Quanh_Đối_Thủ_Có_Đường_Đạn = 7,
    }
}


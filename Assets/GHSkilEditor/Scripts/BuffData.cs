﻿#if UNITY_EDITOR
using System;
using UnityEditor;
#endif
using UnityEngine;

namespace GHTools.SkillCreate
{
    public class BuffData : GHSkillStateData
    {
        public BuffType buffType;
        public float speed;
        public override void ExcuteState()
        {
            if (effectObject == null) return;
            //GameObject buffObj = Instantiate<GameObject>(effectObject);
            BuffPlayer buffCompo = new GameObject("Buff_Controller_", typeof(BuffPlayer)).GetComponent<BuffPlayer>();
            buffCompo.control = gameObject;
        }

        public override void Render(int index, Rect position)
        {
#if UNITY_EDITOR

            EditorGUI.BeginChangeCheck();
            // xuống dòng
            Rect tempPosition = position;
            EditorGUI.LabelField(tempPosition, index + ".Target Type");
            tempPosition = SpaceParameter(position, 2);
            SkillTargetType = (SkillTargetType)EditorGUI.EnumPopup(tempPosition, SkillTargetType);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "First Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountFirst = EditorGUI.FloatField(tempPosition, AmountFirst);
            tempPosition = SpaceParameter(tempPosition, 3);
            EditorGUI.LabelField(tempPosition, "Last Time: ");
            tempPosition = SpaceParameter(tempPosition, 2);
            AmountLast = EditorGUI.FloatField(tempPosition, AmountLast);

            // xuống dòng
            position.y += 25;
            Rect tempPos1 = position;
            EditorGUI.LabelField(tempPos1, new GUIContent(index + ".Buff Type"));
            tempPos1 = SpaceParameter(tempPos1, 2f);
            buffType = (BuffType)EditorGUI.EnumPopup(tempPos1, buffType);
            if (buffType == BuffType.Choáng)
            {
                tempPos1 = SpaceParameter(tempPos1, 3, 120);
                EditorGUI.LabelField(tempPos1, "Buff Object:");
                tempPos1 = SpaceParameter(tempPos1, 2f, 120);
                effectObject = (GameObject)EditorGUI.ObjectField(tempPos1, effectObject, typeof(GameObject), false);
            }
            else if (buffType == BuffType.Hất_Tung)
            {
                tempPos1 = SpaceParameter(tempPos1, 3, 120);
                EditorGUI.LabelField(tempPos1, "Speed:");
                tempPos1 = SpaceParameter(tempPos1, 2f);
                speed = EditorGUI.FloatField(tempPos1, speed);
            }
            else if (buffType == BuffType.Kéo)
            {
                tempPos1 = SpaceParameter(tempPos1, 3, 120);
                EditorGUI.LabelField(tempPos1, "Speed:");
                tempPos1 = SpaceParameter(tempPos1, 2f);
                speed = EditorGUI.FloatField(tempPos1, speed);
            }
            else if (buffType == BuffType.Đẩy)
            {
                tempPos1 = SpaceParameter(tempPos1, 3, 120);
                EditorGUI.LabelField(tempPos1, "Speed:");
                tempPos1 = SpaceParameter(tempPos1, 2f);
                speed = EditorGUI.FloatField(tempPos1, speed);
            }


            if (EditorGUI.EndChangeCheck())
            {
                CheckCondition();
                CheckTime();
            }
#endif
        }

        private void CheckCondition()
        {
            if ((buffType == BuffType.Choáng || buffType == BuffType.Hất_Tung) && SkillTargetType == SkillTargetType.MySelf)
            {
                Debug.LogError("Buff Type này không thể đi chung với Target Type: MySelf. Điều này sẽ tự động chuyển Target thành Enemy.");
                SkillTargetType = SkillTargetType.Enemy;
            }
        }

        public override void Init(GHCreateSkillData gHCreateSkillData)
        {
            base.Init(gHCreateSkillData);
            name = "BuffData_" + UnityEngine.Random.Range(0, 100);
            SkillElementType = SkillElementType.Buff;
            SkillTargetType = SkillTargetType.Enemy;
            buffType = BuffType.Choáng;
            effectObject = Resources.Load<GameObject>("Effect/BuffEffect/buff_stun");
            AmountLast = AmountFirst + 0.5f;
            speed = 2;
        }

    }
}

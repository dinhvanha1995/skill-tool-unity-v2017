﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;

namespace GHTools.SkillCreate
{
    // [System.Serializable]
    public class FunctionData : GHSkillStateData
    {
        //function
        public Transform targetObj;
        public Vector3 targetPositionTemp;
        public string FuntionName;
        public ScriptSkill scriptKill;
        public TypePosition typePos;
        public AxisType typeAxis;
        public Space typeSpace;
        public float offsetX;
        public float offsetY;
        public float offsetZ;
        public float speed;
        public float distance;
        public string des;
        public bool lookAt;
        public bool ignoreLastTime;
        public override void ExcuteState()
        {
            GameObject hero = GameObject.Find(UtilLanguage.HERO);
            if (scriptKill == ScriptSkill.Teleport)
            {
                des = "biến đến vị trí chỉ định.";
                TeleportScript teleport = hero.AddComponent<TeleportScript>();
                teleport.control = gameObject;
            }
            else if (scriptKill == ScriptSkill.Moving)
            {
                des = "lao tới mục tiêu.";
                MovingScript moving = hero.AddComponent<MovingScript>();
                moving.control = gameObject;
            }
            else if (scriptKill == ScriptSkill.Vanish)
            {
                des = "ẩn thân chờ thời cơ.";
                BlindScript blind = hero.AddComponent<BlindScript>();
                blind.control = gameObject;
            }
            else if (scriptKill == ScriptSkill.Rotate)
            {
                des = "xoay quanh bản thân.";
                RotateScript rotate = hero.AddComponent<RotateScript>();
                rotate.control = gameObject;
            }
        }
        public override void Render(int index, Rect position)
        {
#if UNITY_EDITOR
            EditorGUI.BeginChangeCheck();
            //ScriptKill = (ScriptSkill)EditorGUI.ObjectField(position, index + ".Mono", ScriptKill, typeof(MonoBehaviour), false);
            Rect tempPosition = position;
            EditorGUI.LabelField(tempPosition, index + ".MonoBehaviour");
            tempPosition = SpaceParameter(tempPosition, 2.2f, 80);
            scriptKill = (ScriptSkill)EditorGUI.EnumPopup(tempPosition, scriptKill);
            tempPosition = SpaceParameter(tempPosition, 2f);
            EditorGUI.LabelField(tempPosition, "First Time: ");
            tempPosition = SpaceParameter(tempPosition, 2, 60, 35);
            AmountFirst = EditorGUI.FloatField(tempPosition, AmountFirst);
            tempPosition = SpaceParameter(tempPosition, 1.5f);
            EditorGUI.LabelField(tempPosition, "Last Time: ");
            tempPosition = SpaceParameter(tempPosition, 2, 60, 35);
            AmountLast = EditorGUI.FloatField(tempPosition, AmountLast);

            tempPosition = SpaceParameter(tempPosition, 1.5f);
            EditorGUI.LabelField(tempPosition, new GUIContent("Look At: ", "Enable: Khi diễn skill sẽ quay mặt về phía target"));
            tempPosition = SpaceParameter(tempPosition, 2, 50, 25);
            lookAt = EditorGUI.Toggle(tempPosition, lookAt);
            if (scriptKill == ScriptSkill.Moving)
            {
                tempPosition = SpaceParameter(tempPosition, 1.5f, 100, 20);
                EditorGUI.LabelField(tempPosition, new GUIContent("Ignore LastTime: ", "Enable: Player sẽ lướt tới vị trí chỉ định mà không quan tâm tới LastTime.\nDisable: Player sẽ lướt tới vị trí chỉ định trong khoảng thời gian từ FirstTime đến LastTime."));
                tempPosition = SpaceParameter(tempPosition, 2, 70, 50);
                ignoreLastTime = EditorGUI.Toggle(tempPosition, ignoreLastTime);
            }
            // xuống dòng
            position.y += 25;
            Rect tempPosition1 = position;
            EditorGUI.LabelField(tempPosition1, index + ".Parameter: ");
            if (scriptKill != ScriptSkill.Vanish && scriptKill != ScriptSkill.Rotate)
            {
                tempPosition1 = SpaceParameter(tempPosition1, 2);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Target: ", "Create and drag empty gameObject"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 140, 40);
                targetObj = (Transform)EditorGUI.ObjectField(tempPosition1, targetObj, typeof(Transform), true);
                tempPosition1 = SpaceParameter(tempPosition1, 1f);
                tempPosition1 = SpaceParameter(tempPosition1, 2.5f);
                if (targetObj != null)
                {
                    targetPositionTemp = targetObj.position;
                }
            }


            if (scriptKill == ScriptSkill.Teleport)
            {

                EditorGUI.LabelField(tempPosition1, new GUIContent("Offset: ", "Cách 1 khoảng offset so với Target Pos"));
                tempPosition1 = SpaceParameter(tempPosition1, 1, 20);
                EditorGUI.LabelField(tempPosition1, new GUIContent("X"));

                tempPosition1 = SpaceParameter(tempPosition1, 1, 40, 15);
                offsetX = EditorGUI.FloatField(tempPosition1, offsetX);

                tempPosition1 = SpaceParameter(tempPosition1, 1, 20);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Y"));

                tempPosition1 = SpaceParameter(tempPosition1, 1, 40, 15);
                offsetY = EditorGUI.FloatField(tempPosition1, offsetY);

                tempPosition1 = SpaceParameter(tempPosition1, 1, 20);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Z"));

                tempPosition1 = SpaceParameter(tempPosition1, 1, 40, 15);
                offsetZ = EditorGUI.FloatField(tempPosition1, offsetZ);
            }
            else if (scriptKill == ScriptSkill.Moving)
            {
                EditorGUI.LabelField(tempPosition1, new GUIContent("Position: ", "Vị trí sau khi hoàn thành so với Target (Trước mặt, Sau lưng, Trái, Phải)"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 70, 40);
                typePos = (TypePosition)EditorGUI.EnumPopup(tempPosition1, typePos);
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 80, 60);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Distance: ", "Khoảng cách sau khi hoàn thành so với Target"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 50, 38);
                distance = EditorGUI.FloatField(tempPosition1, distance);
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 80, 40);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Speed: ", "Tốc độ di chuyển của Player"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 50, 33);
                speed = EditorGUI.FloatField(tempPosition1, speed);
            }
            else if (scriptKill == ScriptSkill.Rotate)
            {
                tempPosition1 = SpaceParameter(tempPosition1, 2f);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Axis: ", "Trục xoay của Player"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 80, 30);
                typeAxis = (AxisType)EditorGUI.EnumPopup(tempPosition1, typeAxis);

                tempPosition1 = SpaceParameter(tempPosition1, 2.5f, 80, 40);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Speed: ", "Tốc độ xoay của Player"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 50, 33);
                speed = EditorGUI.FloatField(tempPosition1, speed);
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 80, 60);
                EditorGUI.LabelField(tempPosition1, new GUIContent("Space: ", "Rotate with global or local of Object. Default: World"));
                tempPosition1 = SpaceParameter(tempPosition1, 1.5f, 50, 38);
                typeSpace = (Space)EditorGUI.EnumPopup(tempPosition1, typeSpace);

            }
            else if (scriptKill == ScriptSkill.Vanish)
            {

            }
            if (EditorGUI.EndChangeCheck())
            {
                CheckTime();
            }
#endif
        }


        public override void Init(GHCreateSkillData gHCreateSkillData)
        {
            base.Init(gHCreateSkillData);
            name = "FunctionData_" + UnityEngine.Random.Range(0, 100);
            SkillElementType = SkillElementType.Function;
            speed = 1f;
            typeAxis = AxisType.Y;
            typeSpace = Space.World;
            lookAt = true;
            ignoreLastTime = true;
        }
    }
}

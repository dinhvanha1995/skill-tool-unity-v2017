﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GHTools.SkillCreate
{
    [ExecuteInEditMode]
    public class BuffPlayer : MonoBehaviour
    {
        public GameObject control;
        private BuffType buffType;
        private SkillTargetType skillTarget;
        
        Vector3 s_Top;
        private ParticleSystem particleParent;
        private ParticleSystem[] particles;
        private Animator[] animators;
        private ParticleSystem.MainModule main;
        private float time = 0;

        private Vector3 enemyPos;
        public float speed = 1f;
        float timeDelete = 0; // delete this component
        GameObject enemy;
        GameObject hero;
        public void Awake()
        {
            gameObject.name = gameObject.name + UnityEngine.Random.Range(1, 100);
            particleParent = gameObject.GetComponent<ParticleSystem>();
            if (particleParent == null)
            {
                particleParent = gameObject.AddComponent<ParticleSystem>();
            }
        }

        private void SetMainModule()
        {
#if UNITY_EDITOR
            main = particleParent.main;
            if (!UnityEditor.EditorApplication.isPlaying)
            {
                main.duration = timeDelete * 3;
            }
            main.startDelay = 0;
            main.simulationSpeed = 1f;
            main.loop = true;
#endif
        }

        private void SetAnimators()
        {
            animators = gameObject.GetComponentsInChildren<Animator>();
        }


        void Start()
        {
            if (control)
            {
                BuffData buffData = control.GetComponent<BuffData>();
                if (buffData != null)
                {
                    skillTarget = buffData.SkillTargetType;
                    buffType = buffData.buffType;
                    timeDelete = buffData.AmountLast - buffData.AmountFirst;


                    if (buffType == BuffType.Choáng)
                    {
                        if (skillTarget == SkillTargetType.Enemy)
                        {
                            GameObject effectObject = Instantiate<GameObject>(buffData.effectObject);
                         
                            enemy = GameObject.Find(UtilLanguage.ENEMY);
                            gameObject.transform.SetParent(enemy.transform);
                            gameObject.transform.localPosition = Vector3.zero;
                            effectObject.transform.SetParent(gameObject.transform);
                            effectObject.transform.position = AssignTopPosition();
                        }
                    }
                    else if (buffType == BuffType.Hất_Tung)
                    {
                        if (skillTarget == SkillTargetType.Enemy)
                        {
                            enemy = GameObject.Find(UtilLanguage.ENEMY);
                            speed = buffData.speed;
                        }
                    }
                    else if (buffType == BuffType.Kéo || buffType == BuffType.Đẩy)
                    {
                        if (skillTarget == SkillTargetType.Enemy)
                        {
                            enemy = GameObject.Find(UtilLanguage.ENEMY);
                            hero = GameObject.Find(UtilLanguage.HERO);
                            speed = buffData.speed;
                        }
                    }
                }
                SetAnimators();
                SetMainModule();
                SetParticles();
            }
        }

        private Vector3 AssignTopPosition()
        {
            if (enemy != null)
            {
                if (GetChildObjectbyName(enemy.transform, "Top") != null)
                {
                    s_Top = GetChildObjectbyName(enemy.transform, "Top").position;
                }
                else
                {
                    var skinnedMesh = enemy.GetComponentsInChildren<SkinnedMeshRenderer>();
                    float maxY = -999f;

                    foreach (var skinned in skinnedMesh)
                    {
                        if (skinned != null)
                        {
                            Mesh bakedMesh = new Mesh();
                            skinned.BakeMesh(bakedMesh);
                            foreach (var vertex in bakedMesh.vertices)
                            {
                                Vector3 posVertex = skinned.transform.TransformPoint(vertex);
                                maxY = Mathf.Max(maxY, posVertex.y);
                            }
                        }

                    }
                    s_Top = new Vector3(enemy.transform.position.x, maxY, enemy.transform.position.z);
                }

                return s_Top;
            }
            return Vector3.zero;
        }

        public void SetParticles()
        {
            //main.cullingMode = ParticleSystemCullingMode.AlwaysSimulate;
            particleParent.GetComponent<ParticleSystemRenderer>().enabled = false;
            //particle.Simulate(time, true, false, true);
        }

        private Transform GetChildObjectbyName(Transform parent, string childName)
        {
            var queue = new Queue<Transform>();
            queue.Enqueue(parent.transform);
            while (queue.Count > 0)
            {
                var transf = queue.Dequeue();
                if (null != transf && transf.gameObject.name == childName)
                {
                    return transf;
                }
                for (int i = 0; i < transf.childCount; ++i)
                {
                    var child = transf.GetChild(i);
                    queue.Enqueue(child);
                }
            }
            return null;
        }

        void Update()
        {
#if UNITY_EDITOR
            time += Time.deltaTime;
            if (buffType == BuffType.Choáng)
            {
                if (!UnityEditor.EditorApplication.isPlaying)
                {
                    particleParent.Simulate(Time.deltaTime, true, false);
                    if (animators != null && animators.Length > 0)
                    {
                        foreach (var animator in animators)
                        {
                            if (animator != null)
                            {
                                //  Debug.Log(animator.gameObject.name, animator.gameObject);
                                if (animator.gameObject.activeInHierarchy)
                                {
                                    AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                                    animator.Play(stateInfo.fullPathHash);
                                    animator.Update(Time.deltaTime);
                                }
                            }
                        }
                    }
                    //Debug.Log("time = " + time + "   __  " + "timeDelete = " + timeDelete);
                }
                DeleteGameObject(1);
            }
            else if (buffType == BuffType.Hất_Tung)
            {
                if (enemy != null)
                {
                    enemyPos = enemy.transform.position;
                    if (time <= timeDelete / 2)
                    {
                        enemy.transform.Translate(Vector3.up * Time.deltaTime * speed, Space.World);
                    }
                    else
                    {
                        enemy.transform.Translate(-Vector3.up * Time.deltaTime * speed, Space.World);
                    }
                    enemy.transform.position = new Vector3(enemyPos.x, Mathf.Clamp(enemy.transform.position.y, 0, 1000), enemyPos.z);
                    if (time >= (timeDelete + Time.deltaTime * 3))
                    {
                        enemy.transform.position = enemyPos;
                    }
                }
                DeleteGameObject(3);
            }
            else if (buffType == BuffType.Kéo)
            {
                if (enemy != null && hero != null)
                {
                    enemyPos = enemy.transform.position;
                    //var directionNor = (enemy.transform.rotation.eulerAngles - hero.transform.rotation.eulerAngles).normalized;
                    var directionNor = (hero.transform.position - enemy.transform.position).normalized;
                    enemy.transform.Translate(directionNor * Time.deltaTime * speed, Space.World);
                    if (time >= (timeDelete + Time.deltaTime))
                    {
                        enemy.transform.position = enemyPos;
                    }
                }
                DeleteGameObject(1);
            }
            else if (buffType == BuffType.Đẩy)
            {
                if (enemy != null && hero != null)
                {
                    enemyPos = enemy.transform.position;
                    //var directionNor = (enemy.transform.rotation.eulerAngles - hero.transform.rotation.eulerAngles).normalized;
                    var directionNor = (enemy.transform.position - hero.transform.position).normalized;
                    enemy.transform.Translate(directionNor * Time.deltaTime * speed, Space.World);
                    if (time >= (timeDelete + Time.deltaTime))
                    {
                        enemy.transform.position = enemyPos;
                    }
                }
                DeleteGameObject(1);
            }
#endif
        }

        private void DeleteGameObject(int fractor)
        {
            if (time >= (timeDelete + Time.deltaTime * fractor))
            {
                DestroyImmediate(gameObject);
            }
        }

        private void OnDestroy()
        {
        }
    }
}

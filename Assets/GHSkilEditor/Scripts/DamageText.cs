﻿using GHTools.SkillCreate;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class DamageText : MonoBehaviour
{
    public float OffsetHeight = 10;
    public float speed = 0.2f;
    private Vector3 beginPosition;
    private Vector3 targetPosition;
    public GameObject control;
    public int damageFake;
    public Color colorDamage;
    private void Start()
    {
        if (control != null)
        {
            var dameData = control.GetComponent<DamageData>();
            if (dameData != null)
            {
                damageFake = dameData.damageFake;
                colorDamage = dameData.colorDamage;
            }
        }
        var textMesh = gameObject.GetComponentInChildren<TextMesh>();
        if (textMesh != null)
        {
            textMesh.text = damageFake.ToString();
            textMesh.color = colorDamage;
        }
        else
        {
            textMesh.text = "501";
            textMesh.color = Color.red;
        }
        beginPosition = transform.position;
        targetPosition = beginPosition + OffsetHeight * Vector3.up;
    }
    private void Update()
    {
        transform.position = Vector3.MoveTowards(transform.position, targetPosition, speed);
        if(transform.position.y >= targetPosition.y)
        {
            DestroyImmediate(gameObject);
        }
    }
}

﻿#if UNITY_EDITOR
using UnityEditor;
#endif
using UnityEngine;
[ExecuteInEditMode]
public class ParticlePlayer : MonoBehaviour
{
    private ParticleSystem particleParent;
    private ParticleSystem[] particles;
    private Animator[] animators;
    private ParticleSystem.MainModule main;
    public float time = 0;
    public bool bind;
    float timeDelete = 0;
    public static int amountEff = 0;
    public static int maxEff = 10;
    GameObject hero;
    public void Awake()
    {
        gameObject.name = gameObject.name + UnityEngine.Random.Range(1, 100);
        particleParent = gameObject.GetComponent<ParticleSystem>();
        if (particleParent == null)
        {
            particleParent = gameObject.AddComponent<ParticleSystem>();
        }
        SetAnimators();
        SetTimeDelete();
        SetMainModule();
    }

    private void SetAnimators()
    {
        animators = gameObject.GetComponentsInChildren<Animator>();
    }

    public void Start()
    {
        hero = GameObject.Find(UtilLanguage.HERO);
        SetParticles();
    }
    public void SetParticles()
    {
        //main.cullingMode = ParticleSystemCullingMode.AlwaysSimulate;
        particleParent.GetComponent<ParticleSystemRenderer>().enabled = false;
        //particle.Simulate(time, true, false, true);
        SetRotateByHero();
    }

    private void SetMainModule()
    {
#if UNITY_EDITOR
        main = particleParent.main;
        if (!EditorApplication.isPlaying)
        {
            main.duration = timeDelete;
        }
        main.startDelay = 0;
        main.simulationSpeed = 1f;
        main.loop = false;
#endif
    }

    private void SetRotateByHero()
    {
        transform.rotation = hero.transform.rotation;
    }

    private void SetTimeDelete()
    {
        particles = gameObject.GetComponentsInChildren<ParticleSystem>();
        foreach (var ps in particles)
        {
           // Debug.Log(ps.gameObject.name, ps.gameObject);

            if (ps.gameObject.name == this.name) continue;
            ParticleSystem.MainModule main = ps.main;
            timeDelete = Mathf.Max(timeDelete, main.startLifetime.constant + main.duration + main.startDelay.constant);
        }
        foreach (var animator in animators)
        {
            foreach (var clip in animator.runtimeAnimatorController.animationClips)
            {
                timeDelete = Mathf.Max(timeDelete, clip.length);
            }
        }
    }

    private bool SetAnimStop()
    {
        bool stopAnim = true;
        foreach (var ps in particles)
        {
            if (!ps.isStopped)
            {
                stopAnim = false;
            }
        }
        return stopAnim;
    }

    public void Update()
    {
#if UNITY_EDITOR
        if (!EditorApplication.isPlaying)
        {
            time += Time.deltaTime;
            particleParent.Simulate(Time.deltaTime, true, false);

            //particleParent.Simulate(time, true, true);
            //Debug.Log("time: "+time + "  _  ps.time = " + particleParent.time + " _ " + particleParent.gameObject.name + " _ " + particleParent.duration);
            
            //foreach (var particle in particles)
            //{
            //    if (particle.gameObject.activeInHierarchy)
            //    {
            //        particle.Simulate(Time.deltaTime, true, false);
            //    }
            //}
       
            foreach (var animator in animators)
            {
                if (animator != null)
                {
                    //  Debug.Log(animator.gameObject.name, animator.gameObject);
                    if (animator.gameObject.activeInHierarchy)
                    {
                        AnimatorStateInfo stateInfo = animator.GetCurrentAnimatorStateInfo(0);
                        animator.Play(stateInfo.fullPathHash);
                        animator.Update(Time.deltaTime);
                    }

                    //AnimationClip clip = animator.runtimeAnimatorController.animationClips[0];
                    //animator.Play(clip.name);
                    //animator.Update(Time.deltaTime);

                    //AnimationMode.BeginSampling();
                    //AnimationMode.SampleAnimationClip(animator.gameObject, clip, time);
                    //AnimationMode.EndSampling();
                }
            }
        }
        if (hero != null && hero.activeInHierarchy)
        {
            SetRotateByHero();
            if (bind)
            {
                gameObject.transform.position = hero.transform.position;
            }
        }
        if ((time >= timeDelete || SetAnimStop()))
        {
            DestroyImmediate(gameObject);
            amountEff--;
        }
#endif
    }
}
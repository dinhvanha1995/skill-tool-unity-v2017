﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
namespace GHTools.SkillCreate
{
    public class GHSkillStateData : MonoBehaviour, IGHSkillStateData
    {
        public SkillElementType SkillElementType;
        public SkillTargetType SkillTargetType;
        public float AmountFirst;
        public float AmountLast;
        public bool isPlayed;
        public GameObject effectObject;
        public GHCreateSkillData parentControl;
        public virtual void Update()
        {
            //update logic

            //call excute
        }
        public virtual void ExcuteState()
        {
        }
        public void SettingPosEffect(GameObject eff, SkillTargetType skillTarget)
        {
            if (parentControl != null)
            {
                if (parentControl.CharacterData == null)
                {
                    Debug.LogError("Miss Model Character");
                    return;
                }
                if (parentControl.TargetData == null)
                {
                    Debug.LogError("Miss Model Enemy");
                    return;
                }
                if (skillTarget == SkillTargetType.MySelf)
                {
                    eff.transform.position = parentControl.CharacterData.Model.position;
                }
                else if (skillTarget == SkillTargetType.Enemy)
                {
                    eff.transform.position = parentControl.TargetData.Model.position;
                }
            }
            else
            {
                GameObject parent = gameObject.transform.parent.gameObject;
                GHCreateSkillData createSkillData = parent.GetComponent<GHCreateSkillData>();
                if (createSkillData != null)
                {
                    parentControl = createSkillData;
                    SettingPosEffect(eff, skillTarget);
                }
            }
          
        }
        public virtual void Render(int index, Rect position)
        {
        }
        public virtual void Init(GHCreateSkillData parent)
        {
            AmountLast = AmountFirst + 0.2f;
            parentControl = parent;
        }

        public void CheckTime()
        {
            if (AmountFirst < 0)
            {
                AmountFirst = 0;
            }
            if (AmountFirst >= AmountLast)
            {
                AmountLast = AmountFirst + 0.2f;
            }
        }

        public Rect SpaceParameter(Rect position, float ratio, float posMax = 100, float posMin = 50)
        {
            Rect temp = position;
            temp.xMin += posMin * ratio;
            temp.xMax = temp.xMin + posMax;
            return temp;
        }
    }
}

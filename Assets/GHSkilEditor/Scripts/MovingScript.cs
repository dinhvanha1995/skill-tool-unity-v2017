﻿using UnityEngine;


namespace GHTools.SkillCreate
{
    [ExecuteInEditMode]
    public class MovingScript : AbsScript
    {
        float space;
        Vector3 target;
        // Use this for initialization
        private void OnDrawGizmos()
        {
           if (target != null)
            {
                Gizmos.DrawSphere(target, 0.2f);
            }
        }
        void Start()
        {
            AssignVariable();
            if (targetObj == null)
            {
                if (targetPositionTemp != null)
                {
                    gameObject.transform.position = targetPositionTemp;
                    string nameTarget = "Target Position_" + Random.Range(1, 100);
                    GameObject target = new GameObject(nameTarget);
                    target.transform.position = targetPositionTemp;
                    if (control != null)
                    {
                        FunctionData func = control.GetComponent<FunctionData>();
                        if (func != null)
                        {
                            func.targetObj = target.transform;
                            targetObj = target.transform;
                        }
                    }
                }
               
            }
            if (targetObj != null)
            {
                if (type == TypePosition.Front)
                {
                    target = GetPointByVectorAndDistance(targetObj.transform.position, targetObj.transform.forward, distance);
                    space = Vector3.Distance(gameObject.transform.position, target);
                
                } else if (type == TypePosition.Behind)
                {
                    target = GetPointByVectorAndDistance(targetObj.transform.position, targetObj.transform.forward * -1f, distance);
                    space = Vector3.Distance(gameObject.transform.position, target);
                }
                else if (type == TypePosition.Left)
                {
                    target = GetPointByVectorAndDistance(targetObj.transform.position, targetObj.transform.right * -1f, distance);
                    space = Vector3.Distance(gameObject.transform.position, target);
                }
                else if (type == TypePosition.Right)
                {
                    target = GetPointByVectorAndDistance(targetObj.transform.position, targetObj.transform.right, distance);
                    space = Vector3.Distance(gameObject.transform.position, target);
                }
                transform.LookAt(targetObj);
            }
            else
            {
                Debug.LogError(string.Format("Target of Moving = null"));
            }

        }
        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;
            if (targetObj != null)
            {
                if (space > 0)
                {
                    transform.position = Vector3.MoveTowards(transform.position, target, speed);
                    LookAt(gameObject, targetObj.gameObject);
                    space -= speed;
                }
                else
                {
                    LookAt(gameObject, targetObj.gameObject);
                }
            }
            
            //Debug.Log("time = " + time + "  _  (lastTime - firstTime) =" + (lastTime - firstTime));
            if (ignoreLastTime)
            {
                if (gameObject.transform.position == targetObj.gameObject.transform.position)
                {
                    DestroyImmediate(this);
                }
            }
            else
            {
                if (time >= (lastTime - firstTime))
                {
                    DestroyImmediate(this);
                }
            }
           
        }
    }
}

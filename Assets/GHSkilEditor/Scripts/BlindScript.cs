﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GHTools.SkillCreate
{
    [ExecuteInEditMode]
    public class BlindScript : AbsScript
    {
        GameObject hero;
        // Use this for initialization
        void Start()
        {
            AssignVariable();
            hero = GameObject.Find(UtilLanguage.HERO);
            if (hero != null)
            {
                var listMesh = hero.GetComponentsInChildren<Renderer>();
                foreach (var mesh in listMesh)
                {
                    mesh.enabled = false;
                }
            }
        }

        // Update is called once per frame
        void Update()
        {
            time += Time.deltaTime;
            if (time >= (lastTime - firstTime))
            {
                var listMesh = hero.GetComponentsInChildren<Renderer>();
                foreach (var mesh in listMesh)
                {
                    mesh.enabled = true;
                }
                if (time >= (lastTime - firstTime + 0.3f))
                {
                    DestroyImmediate(this);

                }
            }
        }
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace GHTools.SkillCreate
{
    [ExecuteInEditMode]
    public class TeleportScript : AbsScript
    {
        //GameObject hero;
        //GameObject enemy;
        private void Start()
        {
            AssignVariable();
            isAnim = false;
        }
        private void Update()
        {
            time += Time.deltaTime;
            if (!isAnim)
            {
                _Teleport();
                isAnim = true;
            }
            if (time >= (lastTime - firstTime))
            {
                DestroyImmediate(this);
            }
        }

        private void _Teleport()
        {
            if (targetObj != null)
            {
                Vector3 pt = targetObj.position;
                gameObject.transform.position = pt + new Vector3(offsetX, offsetY, offsetZ);

                LookAt(gameObject, targetObj.gameObject);
            }
            else
            {
                if (targetPositionTemp != null)
                {
                    gameObject.transform.position = targetPositionTemp;
                    string nameTarget = "Target Position_" + Random.Range(1, 100);
                    GameObject target = new GameObject(nameTarget);
                    target.transform.position = targetPositionTemp;
                    if (control != null)
                    {
                        FunctionData func = control.GetComponent<FunctionData>();
                        if (func != null)
                        {
                            func.targetObj = target.transform;
                        }
                    }
                    LookAt(gameObject, target);
                }
            }
        }

    }
}

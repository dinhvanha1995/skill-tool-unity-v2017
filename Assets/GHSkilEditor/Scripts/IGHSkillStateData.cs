﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public interface IGHSkillStateData
{
    void ExcuteState();
    void Render(int index, Rect position);
    void Init(GHTools.SkillCreate.GHCreateSkillData gHCreateSkillData);
}